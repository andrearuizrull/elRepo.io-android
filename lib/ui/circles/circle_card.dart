/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import 'package:elRepoIo/routes.dart';
import 'package:elRepoIo/constants.dart';
import 'package:elRepoIo/ui/providers/circles_provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:retroshare_dart_wrapper/utils.dart';
import 'package:retroshare_dart_wrapper/retroshare.dart' as rs;
import 'package:elrepo_lib/repo.dart' as repo;
import 'package:elRepoIo/generated/l10n.dart';

class LittleCircleCard extends StatelessWidget {
  final String circleName;
  final double fontSize;
  LittleCircleCard(this.circleName, {this.fontSize = 12});

  @override
  Widget build(BuildContext context) {
    return Wrap(
        spacing: 5,
        crossAxisAlignment: WrapCrossAlignment.center,
        children: [
          Icon(
            Icons.animation,
            color: LOCAL_COLOR,
            size: fontSize,
          ),
          Text(
            circleName,
            style: TextStyle(fontSize: fontSize, color: LOCAL_COLOR),
          ),
        ]
    );
  }
}


class CircleCard extends ConsumerWidget {
  dynamic data;

  /// [data] and [onrerun] are sent by an updatable future widget.
  /// Check [show_circles.dart] for more info.
  CircleCard(this.data);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return Card(
      elevation: 1.0,
      margin: EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          ListTile(
              leading: data['details']['mAmIAdmin']
                  ? Icon(Icons.vpn_key, color: LOCAL_COLOR)
                  : Icon(Icons.public, color: LOCAL_COLOR),
              title: Text(data['mGroupName']),
              subtitle: Column(
                children: [
                  Align(
                      alignment: Alignment.centerLeft,
                      child:
                      Text(getFormatedTime(data['mPublishTs']['xint64']))),
                  Align(
                      alignment: Alignment.centerLeft,
                      child: Text(data['mGroupId'])),
                ],
              )),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              TextButton(
                onPressed: () async {
                  openCircleDetails(context, data['details']);
                },
                child: Row(
                  children: [
                    Icon(Icons.people),
                    if (!data['details']['mAmIAdmin']) Text(S.of(context).participants),
                  ],
                ),
              ),
              if (data['details']['mAmIAdmin'])
                TextButton(
                  onPressed: () {
                    Navigator.pushNamed(context, '/circleDetails',
                        arguments: CircleArguments(
                            false,
                            data['details'],
                            S.of(context).edit + ' ' + S.of(context).circle,
                            ''));
                  },
                  child: Row(
                    children: [
                      Icon(Icons.settings),
                      Text(S.of(context).edit),
                    ],
                  ),
                ),
              // Leave or join a circle will change after 20 or 30 seconds of accept or revoke the invite
              if (repo.imInvitedToACircle(data['details']['myFlag']['value']))
                TextButton(
                  onPressed: () {
                    rs.RsGxsCircles.requestCircleMembership(data['mGroupId'])
                        .then((val) {
                          ref.read(circlesProvider.notifier).update();
                    });
                    if(!data['details']['mAmIAdmin']) _showAcceptRevokeDialog(context);
                  },
                  child: Row(
                    children: [
                      Icon(Icons.mark_email_unread),
                      Text(S.of(context).circleAcceptInvite),
                    ],
                  ),
                )
              else
                TextButton(
                  onPressed: () {
                    rs.RsGxsCircles.cancelCircleMembership(data['mGroupId'])
                        .then((val) => ref.read(circlesProvider.notifier).update()
                    );
                    if(!data['details']['mAmIAdmin']) _showAcceptRevokeDialog(context);
                  },
                  child: Row(
                    children: [
                      Icon(Icons.logout),
                      Text(S.of(context).leave + ' ' + S.of(context).circle),
                    ],
                  ),
                ),
              const SizedBox(width: 8),
            ],
          ),
        ],
      ),
    );
  }

  void _showAcceptRevokeDialog(BuildContext context) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(S.of(context).circleCardTitle),
          content: Text(S.of(context).circleCardContent),
          actions: [
            TextButton(
              onPressed: () { Navigator.of(context).pop();  },
              child: Text("OK"), // todo(intl)
            )
          ],
        );
      },
    );
  }
}
