/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import 'package:elRepoIo/ui/providers/models.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:elrepo_lib/repo.dart' as repo;

/// Provider that return only circles that you are allowed
///
/// Using `circle['details']['mAmIAllowed']`
final belongedCirclesProvider = StateProvider<List<dynamic>>((ref) =>
    ref.watch(circlesProvider).where(
            (circle) => circle['details']['mAmIAllowed']
    ).toList()
);

final circlesProvider  = StateNotifierProvider<CirclesNotifier, List<dynamic>>((ref) {
  return CirclesNotifier();
});

class CirclesNotifier extends UpdatableNotifier<List<dynamic>> {
  CirclesNotifier(): super([]);

  // todo(kon): this system to prevent update multiple times is repeated on
  //  multiple classes and could be improved centralizing the system somehow on
  // [UpdatableNotifier] class
  bool _alreadyUpdating = false;
  @override
  bool get alreadyUpdating => _alreadyUpdating;
  @override
  set alreadyUpdating (bool val)=> _alreadyUpdating = val;
  @override
  Future<void> update() async {
    if(!alreadyUpdating) {
      alreadyUpdating = true;
      await repo
          .getCirclesDetails(); // First, you need to update circle details backend
      repo.getSortedCircles().then((value) {
        // todo(kon): this is always true!
        if (!listEquals(state, value)) {
          state = value;
        }
        alreadyUpdating = false;
      });
    }
  }
}