/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */

// Extracted from https://github.com/JulianAssmann/flutter_background/ Thanks!

/// Represents the importance of an android notification as described
/// under https://developer.android.com/training/notify-user/channels#importance.
enum AndroidNotificationImportance {
  Default,
  Min,
  Low,
  High,
  Max,
}

// Represents the information required to get an Android resource.
// See https://developer.android.com/reference/android/content/res/Resources for reference.
class AndroidResource {
  // The name of the desired resource.
  final String name;

  // Optional default resource type to find, if "type/" is not included in the name. Can be null to require an explicit type.
  final String defType;

  const AndroidResource(this.name, {this.defType = 'drawable'});
}

/// Android configuration for the [FlutterRetroshareService] plugin.
class FlutterRetroshareServiceAndroidConfig {
  /// The importance of the notification used for the foreground service.
  final AndroidNotificationImportance notificationImportance;

  /// The title used for the foreground service notification.
  final String notificationTitle;

  /// The body used for the foreground service notification.
  final String notificationText;

  /// The resource name of the icon to be used for the foreground notification.
  final AndroidResource notificationIcon;

  /// Creates an Android specific configuration for the [FlutterRetroshareService] plugin.
  ///
  /// [notificationTitle] is the title used for the foreground service notification.
  /// [notificationText] is the body used for the foreground service notification.
  /// [notificationImportance] is the importance of the foreground service notification.
  /// [notificationIcon] must be a drawable resource.
  /// E. g. if the icon with name "background_icon" is in the "drawable" resource folder,
  /// [notificationIcon] should be of value `AndroidResource(name: 'background_icon', defType: 'drawable').
  /// It must be greater than [AndroidNotificationImportance.Min].
  const FlutterRetroshareServiceAndroidConfig(
      {
      // Not working, hardcoded on the android code
      this.notificationTitle = 'elRepo.io', //todo(intl)
      // Not working, hardcoded on the android code
      this.notificationText =
          'This notification keeps elRepo.io alive', //todo(intl)
      this.notificationImportance = AndroidNotificationImportance.Default,
      this.notificationIcon =
          const AndroidResource('ic_notification_outerborder', defType: 'drawable')});
}
