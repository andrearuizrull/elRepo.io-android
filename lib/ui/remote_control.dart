/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:retroshare_dart_wrapper/retroshare.dart' as rs;
import 'package:elrepo_lib/repo.dart' as repo;

import 'package:elRepoIo/constants.dart';
import 'package:elRepoIo/generated/l10n.dart';

class RemoteControlScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return RemoteControl();
  }
}

class RemoteControl extends StatefulWidget {
  @override
  _RemoteControlState createState() => _RemoteControlState();
}

class _RemoteControlState extends State<RemoteControl> {
  final prefixUrlETController = TextEditingController();

  bool _errorMsgVisible = false;
  String _prefixInfoText = 'Checking';

  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      _prefixInfoText = _getPrefixInfoText();
      setState(() {});
    });
    super.initState();
  }

  // Info text that tell if you are connected local o remotelly
  String _getPrefixInfoText() => rs.getRetroshareServicePrefix() ==
      rs.RETROSHARE_SERVICE_PREFIX
      ? S.of(context).remoteControlLocallyConnected
      : S.of(context).remoteControlConnectedTo(rs.getRetroshareServicePrefix());

  // Error message when can't connect to remote
  void _showErrorMessage(bool show) {
    setState(() {
      _errorMsgVisible = show;
    });
  }

  void _setRemoteControl() {
    _showErrorMessage(false);
    setState(() {
      _prefixInfoText = _getPrefixInfoText();
    });
  }

  void _disconnectRemoteControl() {
    repo.disableRemoteControl();
    setState(() {
      _prefixInfoText = _getPrefixInfoText();
    });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
          appBar: AppBar(
            backgroundColor: PURPLE_COLOR.withOpacity(0.9),
            shadowColor: REMOTE_COLOR,
            brightness: Brightness.dark,
            elevation: 0,
            iconTheme: IconThemeData(color: Colors.black),
            leading: IconButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              icon: Icon(
                Icons.arrow_back_rounded,
                color: WHITE_COLOR,
              ),
            ),
            title: Text(
              S.of(context).remoteControl,
              style: TextStyle(
                fontWeight: FontWeight.w800,
                color: WHITE_COLOR,
              ),
            ),
          ),
          body: Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            child: Center(
              child: SingleChildScrollView(
                child: Padding(
                  padding: EdgeInsets.symmetric(
                    horizontal: 32,
                    vertical: 12,
                  ),
                  child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          _prefixInfoText,
                          style: TextStyle(
                            color: ACCENT_COLOR,
                          ),
                        ),
                        TextField(
                          controller: prefixUrlETController,
                          keyboardType: TextInputType.text,
                          decoration: InputDecoration(
                            hintText: 'http://localhost:8888/rsremote',
                            icon: Icon(Icons.settings_remote),
                          ),
                        ),
                        SizedBox(height: MediaQuery.of(context).size.height * .03),
                        SizedBox(
                          height: MediaQuery.of(context).size.height * .03,
                        ),
                        Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Container(
                                child: RaisedButton(
                                  color: LOCAL_COLOR,
                                  disabledColor: ACCENT_COLOR,
                                  elevation: 4.0,
                                  splashColor: REMOTE_COLOR,
                                  onPressed: () async {
                                    await repo.enableRemoteControl(
                                        prefixUrlETController.text,
                                        rs.RETROSHARE_API_USER)
                                        ? _setRemoteControl()
                                        : _showErrorMessage(true);
                                  },
                                  child: Text(
                                    S.of(context).connect,
                                    style: TextStyle(
                                      color: WHITE_COLOR,
                                    ),
                                  ),
                                ),
                              ),
                              SizedBox(
                                width: MediaQuery.of(context).size.width * .05,
                              ),
                              Container(
                                child: RaisedButton(
                                  color: LOCAL_COLOR,
                                  disabledColor: ACCENT_COLOR,
                                  elevation: 4.0,
                                  splashColor: REMOTE_COLOR,
                                  onPressed: () async {
                                    _disconnectRemoteControl();
                                  },
                                  child: Text(
                                    S.of(context).disconnect,
                                    style: TextStyle(
                                      color: WHITE_COLOR,
                                    ),
                                  ),
                                ),
                              ),
                            ]
                        ),
                        SizedBox(height: MediaQuery.of(context).size.height * .03),
                        Text(
                          'Experimental: this screen is used to connect to remote nodes with remote control enabled'
                              "\nDon't use it if you don't know what are you doing!", // todo(intl)
                          style: TextStyle(
                            color: REMOTE_COLOR,
                          ),
                        ),
                        SizedBox(height: MediaQuery.of(context).size.height * .03),
                        Text(
                          S.of(context).remoteRules,
                          style: TextStyle(
                            color: REMOTE_COLOR,
                          ),
                        ),
                        Visibility(
                          visible: _errorMsgVisible,
                          child: Text(
                            S.of(context).remoteControlerror,
                            style: TextStyle(
                              color: Colors.redAccent,
                            ),
                          ),
                        )
                      ]),
                ),
              ),
            ),
          ),
        ));
  }
}
