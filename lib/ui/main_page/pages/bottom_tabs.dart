/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import 'package:elRepoIo/routes.dart' show TabsPageArguments;
import 'package:elRepoIo/constants.dart';
import 'package:flutter/material.dart';
import 'package:elRepoIo/ui/main_page/models/tab_navigation_item.dart';

class TabsPage extends StatefulWidget {
  TabsPageArguments tabsPageArguments;

  TabsPage({Key key, this.tabsPageArguments}) : super(key: key);

  @override
  _TabsPageState createState() => _TabsPageState();
}

class _TabsPageState extends State<TabsPage> {
  int _currentIndex = 0;

  final List<TabNavigationItem> _tabItems = TabNavigationItem.items;

  @override
  void initState() {
    if(widget.tabsPageArguments != null) {
      _currentIndex = widget.tabsPageArguments.tabIndex ?? TabNavigationItem.items.indexWhere(
              (element) => element.key == 'publish');
    }
    super.initState();
  }

  void _changeToIndex(int newIndex) =>
      setState(() => _currentIndex = newIndex);


  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      // Used to navigate to first tab when back button is pressed
      onWillPop: () async {
        if(_currentIndex != 0 ) {
          _changeToIndex(0);
          return false;
        }
        return true;
      },
      child: Scaffold(
        body: IndexedStack(
          index: _currentIndex,
          children: <Widget>[
            for (final tabItem in _tabItems) tabItem.page(widget.tabsPageArguments),
          ],
        ),
        bottomNavigationBar: BottomNavigationBar(
          //backgroundColor: WHITE_COLOR,

          type: BottomNavigationBarType.fixed,
          selectedItemColor: LOCAL_COLOR,
          unselectedItemColor: Colors.grey,
          currentIndex: _currentIndex,
          onTap: (int index) => _changeToIndex(index),
          items: <BottomNavigationBarItem>[
            for (final tabItem in _tabItems)
              BottomNavigationBarItem(
                icon: tabItem.icon,
                label: tabItem.title,
              ),
          ],
        ),
      ),
    );
  }
}
