/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import 'dart:convert';

import 'package:retroshare_dart_wrapper/rs_models.dart';
import 'package:retroshare_dart_wrapper/retroshare.dart' as rs;

/// This file is used to mock the API answers for testings
class MockRsMsgMetaData {
  /// The metadata of a message found in the explore screen
  static var exploreScreenMetadata = RsMsgMetaData.fromJson(
    jsonDecode('''
            {
            "mGroupId": "0546f631d1d5eced298a98cc3d7b8897",
            "mMsgId": "d5846dccdeb03939cb898e8bd78e327fbbc061ae",
            "mThreadId": "0000000000000000000000000000000000000000",
            "mParentId": "0000000000000000000000000000000000000000",
            "mOrigMsgId": "d5846dccdeb03939cb898e8bd78e327fbbc061ae",
            "mAuthorId": "4b01ba1ab1ac85ef10cef012ea8b937e",
            "mMsgName": "{\\"title\\":\\"habita percept\\",\\"summary\\":\\"\\",\\"markup\\":\\"plain\\",\\"contentType\\":\\"image\\",\\"role\\":\\"post\\",\\"referred\\":null,\\"circle\\":null}",
            "mPublishTs": {
                "xint64": 1627465582,
                "xstr64": "1627465582"
            },
            "mMsgFlags": 0,
            "mMsgStatus": 7,
            "mChildTs": {
                "xint64": 0,
                "xstr64": "0"
            },
            "mServiceString": ""
        }
    ''')
  );
}


class AUTH {
  static const String identityId = "bdbd397cf7800c5e085968e185633b50";
  static const String locationId = "814228577bc0c5da968c79272adcbfce";
  static const String passphrase = "test";
  static const String apiUser = "test";

  static initLocal() =>
      rs.initRetroshare(
          identityId: AUTH.identityId,
          locationId: AUTH.locationId,
          passphrase: AUTH.passphrase,
          apiUser: AUTH.apiUser
      );

  static initRemote() {
    rs.setRetroshareServicePrefix("http://127.0.0.1:9091");
    rs.initRetroshare(
      identityId: "4b01ba1ab1ac85ef10cef012ea8b937e",
      locationId: AUTH.locationId,
      passphrase: "1234",
    );
  }
}
