/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import 'dart:io';

import 'package:flutter_test/flutter_test.dart';
import 'package:elRepoIo/RsServiceControl/rs_folders_control.dart' as RsFolders;
import 'package:path/path.dart' as path;
import 'package:integration_test/integration_test.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:elrepo_lib/constants.dart' as cnst;

import 'package:elRepoIo/RsServiceControl/rscontrol.dart' as rsControl;
import 'package:retroshare_dart_wrapper/retroshare.dart' as rs;
import 'utils.dart' as test_utils;
@TestOn('android')
void main() {
  IntegrationTestWidgetsFlutterBinding.ensureInitialized();

  final testFileName = 'test';

  Future<void> mockRsFolder () async {
    print('MOCK - .retrohsare/test');
    final appDocDir = await getApplicationDocumentsDirectory();
    final pathComponents = path.split(appDocDir.path);
    pathComponents.removeLast();
    final rsPath = path.join(path.joinAll(pathComponents), '.retroshare');
    File('$rsPath/$testFileName').create(recursive: true);
  }

  group('Tests group for generating backups', () {
    setUp(() {
      mockRsFolder();
    });

    testWidgets('test_correct_path', (WidgetTester tester) async {
      final rsFolder = await RsFolders.RsFoldersControl.getRetrosharePath();
      print('rs folder: $rsFolder');
      assert('.retroshare' == basename(rsFolder));
    });
  });

  group('Tests group for create backups', () {

    setUp(() async {
      await test_utils.rsInitialization();
      await test_utils.requestFilePermissions();
      await test_utils.signup();
    });

    testWidgets('create_backup', (WidgetTester tester) async {
      try{
        final sharedDirs = await rs.RsFiles.getSharedDirectories();
        await rsControl.RsServiceControlPlatform.instance.stopRetroShareExecution();
        final backupZipPath = await RsFolders.RsBackup.createZipBackup(sharedDirs);
        print('Backup created at $backupZipPath');
        assert(await File(backupZipPath).exists());
      } catch (e) {
        print(e);
        rethrow;
      }
    });
  });


  group('Tests group for restore backups from file', () {
    final backupZip = '/storage/emulated/0/Download/elRepo_backup.zip';
    setUp(() async {
      assert(await File(backupZip).exists());
      await test_utils.rsInitialization();
      await test_utils.requestFilePermissions();
    });

    testWidgets('restore_backup', (WidgetTester tester) async {
      try{
        await RsFolders.RsBackup.restoreBackup(backupZip,
                (accountStatus) {
              print('Backup restored $accountStatus');
              assert(accountStatus == cnst.AccountStatus.hasLocation);
            });
      } catch (e) {
        print(e);
        rethrow;
      }
    });
  });
}