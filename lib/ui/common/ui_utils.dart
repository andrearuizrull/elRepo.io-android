/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import 'package:elRepoIo/ui/common/updatable_future_builder.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'dart:async';
import 'package:elrepo_lib/models.dart' as models;
import 'package:elrepo_lib/constants.dart' as constants;
import 'package:flutter/services.dart';
import '../../constants.dart';
import 'package:retroshare_dart_wrapper/utils.dart' as utils;
import 'package:retroshare_dart_wrapper/retroshare.dart' as rs
    show
    RsJsonApi,
    authIdentityId,
    authLocationId,
    authLocationName,
    getAuthApiUser,
    getRetroshareServicePrefix;

/// Widget that shows xint64 time to user readable format
///
/// Is basically a text widget with some styles. It use [IconTextWrap]
class TimeText extends  StatelessWidget{
  double fontSize;
  int timestamp;
  IconData iconData;
  FontStyle fontStyle;
  FontWeight fontWeight;
  bool showIcon;
  TimeText(this.timestamp,
      {this.fontSize = 12.0,
        this.showIcon = true,
        this.iconData = Icons.date_range,
        this.fontStyle = FontStyle.italic,
        this.fontWeight = FontWeight.w300});

  @override
  Widget build(BuildContext context) {
    return IconTextWrap(utils.getFormatedTime(timestamp),
        fontSize: fontSize,
        showIcon: showIcon,
        iconData: iconData,
        fontStyle: fontStyle,
        fontWeight: fontWeight);
  }
}

/// Show an icon and then a text
class IconTextWrap extends StatelessWidget {
  double fontSize;
  String txt;
  IconData iconData;
  FontStyle fontStyle;
  FontWeight fontWeight;
  bool showIcon;

  IconTextWrap(this.txt,
      {this.fontSize = 12.0,
        this.showIcon = true,
        this.iconData = Icons.date_range,
        this.fontStyle = FontStyle.italic,
        this.fontWeight = FontWeight.w300});

  @override
  Widget build(BuildContext context) {
    return RichText(
        text: TextSpan(
            style: DefaultTextStyle.of(context).style,
            children: [
              if(showIcon)
                WidgetSpan(
                  alignment: PlaceholderAlignment.middle,
                  child: Icon(
                    iconData,
                    size: fontSize,
                  ),),
              TextSpan(
                text: " $txt",
                style: TextStyle(
                  fontStyle: fontStyle, fontWeight: fontWeight, fontSize: fontSize,
                ),
              ),
            ]
        )
    );
  }
}

Widget loadingBox([String loadingMsg = 'Loading ...']) {
  return Center(
    child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          SizedBox(
            width: 60,
            height: 60,
            child: CircularProgressIndicator(),
          ),
          Padding(
            padding: EdgeInsets.only(top: 15),
            child: Text(loadingMsg),
          )
        ]),
  );
}

/// Get [IconData] widget for an specific [postMetadata.contentType]
IconData getContentTypeIcon(String contentType) =>
    contentTypeIcons.containsKey(contentType)
        ? contentTypeIcons[contentType]
        : contentTypeIcons[models.ContentTypes.text];

Map<String, IconData> contentTypeIcons = {
  models.ContentTypes.image: Icons.image,
  models.ContentTypes.audio: Icons.audiotrack,
  models.ContentTypes.video: Icons.local_movies,
  models.ContentTypes.document: Icons.insert_drive_file,
  models.ContentTypes.file: Icons.attach_file,
  models.ContentTypes.text: Icons.format_align_left
};

/// Text Widget that change the opacity blinking
class OpacityBlinkingText extends StatefulWidget {
  Duration duration;
  var color = Colors.black;
  Text textWidget;

  OpacityBlinkingText(this.textWidget,
      {Key key, this.duration = const Duration(seconds: 1)})
      : super(key: key);
  @override
  _OpacityBlinkingTextState createState() => _OpacityBlinkingTextState();
}

class _OpacityBlinkingTextState extends State<OpacityBlinkingText> {
  double _opacity = 1.0;
  Text _text;
  void changeOpacity() {
    Future.delayed(widget.duration, () {
      if (mounted) {
        setState(() {
          _opacity = _opacity == 0.0 ? 1.0 : 0.0;
          changeOpacity();
        });
      }
    });
  }

  @override
  void initState() {
    super.initState();
    changeOpacity();
    _text = widget.textWidget;
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedOpacity(
        opacity: _opacity, duration: widget.duration, child: _text);
  }

  @override
  dispose() {
    super.dispose();
  }
}

/// Return a RichText widget that has the first string in bold and the second
/// normal.
///
/// Result like: "**Version number: **  1.0"
class BoldAndNormalRichText extends StatelessWidget {
  final String boldString;
  final String normalString;
  BoldAndNormalRichText(this.boldString, this.normalString);
  @override
  Widget build(BuildContext context) {
    return RichText(
      text: TextSpan(
        text: boldString,
        style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black),
        children: <TextSpan>[
          TextSpan(
            text: normalString,
            style:
            TextStyle(fontWeight: FontWeight.normal, color: Colors.black),
          )
        ],
      ),
    );
  }
}

/// Show dialog with util information such version and other
///
/// Rs version extracted from `/RsJsonApi/version`
///
/// Other data extracted from:
/// ```
/// import 'package:retroshare_dart_wrapper/retroshare.dart' as rs show
/// RsJsonApi, authIdentityId, authLocationId, authLocationName, getAuthApiUser,
/// getRetroshareServicePrefix;
/// ```
void showVersionDialog(BuildContext context) async {
  var rsVersion = await rs.RsJsonApi.version();
  var apkVersion = await _getGitInfo();
  showDialog<String>(
    barrierDismissible: false,
    context: context,
    builder: (BuildContext context) => AlertDialog(
      title: const Text(
        'Version', // Todo(intl)
        // style: TextStyle(color: Colors.black),
      ),
      content: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          // authIdentityId, authLocationId, authLocationName, authApiUser, getRetroshareServicePrefix
          BoldAndNormalRichText('RetroShare version: ', rsVersion['human']),
          BoldAndNormalRichText('elRepo.io version: ', constants.API_VERSION),
          BoldAndNormalRichText('Location Id: ', rs.authLocationId),
          BoldAndNormalRichText('Location name: ', rs.authLocationName),
          BoldAndNormalRichText('Identity: ', rs.authIdentityId),
          BoldAndNormalRichText('Prefix: ', rs.getRetroshareServicePrefix()),
          BoldAndNormalRichText('authApiUser: ', rs.getAuthApiUser()),
          BoldAndNormalRichText('commit: ', '${apkVersion.first.substring(0, 7)} ${apkVersion.last}' ),
        ],
      ),
      actions: <Widget>[
        TextButton(
          onPressed: () {
            Navigator.pop(context, 'OK');
          },
          child: const Text('OK'), // Todo(intl)
        ),
      ],
    ),
  );
}

/// Get git commit and branch name
Future<Set<String>> _getGitInfo() async {
  final branch = (await rootBundle.loadString('.git/HEAD')).split('/').last.trim();
  final commit = await rootBundle.loadString('.git/refs/heads/$branch');
  return <String>{commit, branch};
}

/// Widget that once built, and after a delay, run a 
/// [UpdatableFutureBuilderController.runFuture] function. Used to automatically 
/// update an [UpdatableFutureBuilder] after a [delay]. 
///
/// You have to pass this widget as `refreshButton` in an
/// [UpdatableFutureBuilder] and pass the controller on both: to this widget and 
/// to the future builder.
/// 
/// Note: I tried to just pass the `UpdatableFutureBuilderController.runFuture` 
/// as function but the `runFuture` function is `null` until the 
/// [UpdatableFutureBuilder] do the `initState`, so it remain `null` in this 
/// widget. If in the future I want to create a more generic funcion, create a 
/// mixin from this one.
class FutureBuilderAutoUpdate extends StatefulWidget {
  final int delay;
  final UpdatableFutureBuilderController controller;
  final Widget child;
  const FutureBuilderAutoUpdate(this.controller, {Key key, this.delay = 2, this.child}) : super(key: key);

  @override
  _FutureBuilderAutoUpdateState createState() => _FutureBuilderAutoUpdateState();
}

class _FutureBuilderAutoUpdateState extends State<FutureBuilderAutoUpdate> {
  Timer _t;
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      if(!widget.controller.hasData) {
        _t = Timer(Duration(seconds: widget.delay), () {
          widget.controller.runFuture();
          if(widget.controller.hasData) _t.cancel();
        });
      }
    });
  }
  @override
  void dispose() {
    _t?.cancel();
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    return Container(child: widget.child);
  }
}

void confirmDialog(BuildContext context, {
  String title = 'Please Confirm', String accept = 'Yes', String cancel = 'No',
  Widget content = const Text('Are you sure to remove the box?'),
  Function(bool) callback,
}) {
  showDialog(
      context: context,
      builder: (BuildContext ctx) {
        return AlertDialog(
          title: Text(title),
          content: content,
          actions: [
            // The "Yes" button
            TextButton(
                onPressed: () {
                  Navigator.of(context).pop();
                  callback(true);
                },
                child: Text(accept)),
            TextButton(
                onPressed: () {
                  // Close the dialog
                  Navigator.of(context).pop();
                  callback(false);
                },
                child: Text(cancel))
          ],
        );
      });
}

/// Basically same structure as Progress dialog but without context playing to
/// change messages.
///
/// Util for one message circular progress dialogs
void simpleProgressDialog(BuildContext context, {bool onWillPop = false, bool barrierDismissible = false, List<Widget> children}){
  showDialog(
    context: context,
    barrierDismissible: barrierDismissible,
    builder: (c) {
      // return WillPopScope(
      // onWillPop: () async =>
      // false, // This will prevent to close dialog with back button during the execution
      return SimpleDialog(
          elevation: 10.0,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(10.0))),
          children: [
            Center(
                child: Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Column(mainAxisSize: MainAxisSize.min, children: [
                    CircularProgressIndicator(),
                    SizedBox(
                      height: 10,
                    ),
                    ...children
                  ]),
                ))]
        // )
      );
    },

  );
}


/// Used to set state of a switch tile on widgets like bottom sheet or modals
class StatefulSwitchTile extends StatefulWidget {
  final String text;
  final initialValue;
  final Function(bool) onChanged;
  final Widget secondary;
  StatefulSwitchTile(this.text, {this.secondary, this.onChanged, this.initialValue = false});
  @override
  StatefulSwitchTileState createState() => StatefulSwitchTileState();
}
class StatefulSwitchTileState extends State<StatefulSwitchTile> {
  bool _isSwitched = false;
  void _onSwitchChanged(bool value) {
    _isSwitched = value;
  }
  @override
  void initState() {
    _isSwitched = widget.initialValue;
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Container(
      child: SwitchListTile(
        inactiveTrackColor: REMOTE_COLOR,
        activeColor: LOCAL_COLOR,
        contentPadding: const EdgeInsets.all(0),
        secondary: widget.secondary,
        title:Text(widget.text), // just a custom font, otherwise a regular Text widget
        value: _isSwitched,
        onChanged: (bool value){
          setState(() {
            _onSwitchChanged(value);
            if(widget.onChanged!=null) widget.onChanged(value);
          });
        },
      ),
    );
  }
}

/// Button that shows `no data yet` message and have a callback action
class NoDataYetButton extends StatelessWidget {
  final Function callback;
  const NoDataYetButton({@required this.callback, Key key})
      : assert(callback != null), super(key: key);

  @override
  Widget build(BuildContext context) {
    return ElevatedButton.icon(
      style: ButtonStyle(
          backgroundColor: MaterialStateProperty.all(REMOTE_COLOR)),
      label: Text('No data yet!'),
      icon: Icon(Icons.refresh_outlined),
      onPressed: callback,
    );
  }
}

Container buildDivider() {
  return Container(
    margin: const EdgeInsets.symmetric(horizontal: 8.0),
    width: double.infinity,
    height: 1.0,
    color: Colors.grey.shade400,
  );
}

void copyToClipboard(BuildContext context, String toCopy, String snackBarText) {
  ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(content: Text(snackBarText),
      ));
  Clipboard.setData(ClipboardData(text: toCopy));
}