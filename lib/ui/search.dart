/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import 'dart:async';
import 'dart:io';

import 'package:elRepoIo/routes.dart' show SearchArguments;
import 'package:elRepoIo/ui/common/language_codes.dart';
import 'package:elRepoIo/ui/common/posts_teaser.dart';
import 'package:elRepoIo/generated/l10n.dart';
import 'package:elrepo_lib/models.dart';
import 'package:retroshare_dart_wrapper/rs_models.dart' show RsMsgMetaData;
import 'package:flutter/material.dart';
import 'package:elrepo_lib/repo.dart' as repo;
import 'package:retroshare_dart_wrapper/utils.dart' as repoUtils;
import '../constants.dart';
import 'common/search_bar.dart';
import 'common/ui_utils.dart';

class SearchScreen extends StatefulWidget {
  final SearchArguments args;

  const SearchScreen(
    this.args, {
    Key key,
  }) : super(key: key);

  @override
  _SearchScreenState createState() => _SearchScreenState();
}

class _SearchScreenState extends State<SearchScreen>
    with TickerProviderStateMixin {
  // Text editing controller for the search bar
  final TextEditingController _teController = TextEditingController();

  // Subscription to rsEvents
  List<StreamSubscription> rsEvents = <StreamSubscription>[];

  // Controll searching state
  bool _isSearching = false;

  // Height of advanced search acordion
  double _advancedSearchHeight = 0.0;
  final double _advancedMaxHeight = 300.0;

  // Used to store distant search results
  List<RsMsgMetaData> resultsSearchList = [];

  // Used to animate advanced search icon
  AnimationController _animationController;
  bool isPlaying = false;

  // Used for searchable languages dropdown
  var selectedLangs = <String>[];
  var selectedLangsIndex = <int>[];

  /// Image path to perform a preceptual search
  String _imageSearchHash;

  /// Create a list of widgets that will show the search results in a
  /// [postTeaserCard].
  List<Widget> _createDistantList() {
    return List<Widget>.generate(resultsSearchList.length, (int index) {
      return resultsSearchList[index].rawJson['distance'] == null
          ? PostTeaserCard(resultsSearchList[index])
          : Column(
              children: [
                Text(S.of(context).matchPercent((100 -
                            repoUtils.getPerceptualSearchPercent(
                                resultsSearchList[index].rawJson['distance']))
                        .toString()) +
                    '\n'),
                PostTeaserCard(resultsSearchList[index])
              ],
            );
    });
  }

  /// Use distant search
  ///
  /// You can override text editor search string using [searchString].
  /// For perceptual search, you can add a [distance] to show the difference
  /// between the original search file and the result.
  Future<void> _distantSearchGenerator(
      [String searchString, int distance]) async {
    rsEvents.add(
        await repo.distantSearch(searchString ?? _searchString(), (results) {
      repo.deleteFromPostList(resultsSearchList, results);
      for (var result in results) {
        // For some reason distant search return circled results. Prevent to show it
        if(repo.iBelongToACircle(PostMetadata.fromJsonString(result.mMsgName).circle)) {
          result.rawJson['distance'] = distance;
          resultsSearchList.add(result);
        }
      }
      setState(() {});
    }));
  }

  /// Launch search based on [_teController] or overrride by String.
  ///
  /// You can override text editor search string using [searchString].
  /// It search using localSearch first and then using distantSearch.
  /// For perceptual search, you can add a [distance] to show the difference
  /// between the original search file and the result.
  Future<void> _stringSearchGenerator(
      [String searchString, int distance]) async {
    _distantSearchGenerator(searchString, distance);
    var res = await repo.sortedLocalSearch(searchString ?? _searchString());
    if (distance != null) {
      res.forEach((element) {
        element.rawJson['distance'] = distance;
      });
    }

    resultsSearchList.addAll(res);
    setState(() {
      _isSearching = false;
    });
  }

  /// This function is called when you try to search similar images using
  /// perceptual search.
  ///
  /// A rsEvent is used to search for hashes of similar files, called from
  /// [repo.perceptualSearch]. For each resulting hash, a usual
  /// [_stringSearchGenerator] function is called.
  ///
  /// The [_stringSearchGenerator] will do a local and distant search of the
  /// hash of the similar images, getting posts where this hash appear (on the
  /// post metadata [RsMsgMetaData]).
  Future<void> _pereceptualSearchGenerator() async {
    rsEvents.add(await repo.perceptualSearch(widget.args.fileInfo['path'],
        distance: 20, callback: (results) {
      for (var result in results) {
        _stringSearchGenerator(result['fHash'], result['distance']);
      }
    }));
  }

  /// This function is called each time search button is pressed
  Future<void> searchAction(TextEditingController _teController) async {
    setState(() {
      resultsSearchList = <RsMsgMetaData>[];
      _isSearching = true;
    });
    if (_imageSearchHash == null) {
      _stringSearchGenerator();
    } else {
      _pereceptualSearchGenerator();
    }
  }

  // Return a search string that is a combination of the search text edit and
  // advanced search options, languages for example:
  // "text edit string" AND es AND castellano  AND español
  String _searchString() {
    var searchString = _teController.text;
    if(_imageSearchHash == null){ // Which mean that is not a distant search, add wildcards for each word
      searchString = searchString.split(' ').map((x) => '${x}*').toList().join(' ');
    }
    if (selectedLangs.isNotEmpty) {
      if (searchString.isNotEmpty) searchString += ' OR ';
      searchString += getLanguagesNamesFromCodeList(selectedLangs).join(' OR ');
    }
    return searchString;
  }

  @override
  void dispose() {
    rsEvents?.forEach((event) => event?.cancel());
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    if (widget.args?.fileInfo != null) {
      _imageSearchHash = widget.args.fileInfo['hash'];
      _teController.text = _imageSearchHash;
      WidgetsBinding.instance
          .addPostFrameCallback((_) => searchAction(_teController));
    }
    _animationController =
        AnimationController(vsync: this, duration: Duration(milliseconds: 450));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: PURPLE_COLOR.withOpacity(0.9),
        shadowColor: REMOTE_COLOR,
        brightness: Brightness.dark,
        elevation: 0,
        leading: IconButton(
          onPressed: () {
            Navigator.of(context).pop();
          },
          icon: Icon(
            Icons.arrow_back_rounded,
            color: WHITE_COLOR,
          ),
        ),
        title: Text(
          S.of(context).search,
          style: TextStyle(fontWeight: FontWeight.w800, color: WHITE_COLOR),
        ),
      ),
      body: Column(
        children: [
          Row(
            children: [
              Expanded(
                  child: SearchBar(
                teController: _teController,
                searchAction: searchAction,
                enabled: _imageSearchHash == null,
              )),
              Container(
                padding: EdgeInsets.all(
                  10.0,
                ),
                child: IconButton(
                  icon: AnimatedIcon(
                    icon: AnimatedIcons.menu_close,
                    progress: _animationController,
                    color: REMOTE_COLOR,
                  ),
                  onPressed: () {
                    setState(() {
                      _advancedSearchHeight =
                          _advancedSearchHeight == 0
                              ? _advancedMaxHeight
                              : 0.0;
                      isPlaying = !isPlaying;
                      isPlaying
                          ? _animationController.forward()
                          : _animationController.reverse();
                    });
                  },
                ),
              ),
            ],
          ),
          Visibility(
            visible: _advancedSearchHeight != 0,
            child: Card(
              child: AnimatedContainer(
                duration: Duration(seconds:1),
                margin: EdgeInsets.symmetric(horizontal: 10.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    if (_imageSearchHash == null)
                      languagesDropDown(selectedLangsIndex,
                          readOnly: _imageSearchHash != null,
                          onChanged: (indexs, codes) {
                        setState(() {
                          selectedLangsIndex = indexs;
                          selectedLangs = codes;
                        });
                      }),
                    if (_imageSearchHash != null)
                      Image.file(
                        File(widget.args.fileInfo['path']),
                        width: 200,
                      )
                  ],
                ),
                // color: Colors.red,
              ),
            ),
          ),
          SizedBox(
            height: 16,
          ),
          Visibility(
              visible: _isSearching,
              child: loadingBox(S.of(context).searchOverRepoio)),
          Visibility(
              visible: !_isSearching && resultsSearchList.isEmpty,
              child: Text(S.of(context).noResults)),
          Visibility(
            visible: !_isSearching,
            child: Expanded(
              child: ListView(children: [
                for (var distantTeaser in _createDistantList()) distantTeaser
              ]),
            ),
          ),
        ],
      ),
    );
  }
}
