/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import 'package:elRepoIo/ui/providers/models.dart';
import 'package:elRepoIo/ui/providers/teasers_providers.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:retroshare_dart_wrapper/rs_models.dart';
import 'package:elrepo_lib/repo.dart' as repo;

final ownIdentityProvider  = StateNotifierProvider<OwnIdentityNotifier, Identity>((ref) {
  return OwnIdentityNotifier();
});

class OwnIdentityNotifier extends UpdatableNotifier<Identity> {
  OwnIdentityNotifier(): super(Identity('0'));

  void _add(Identity ownId) => state = ownId;

  bool _alreadyUpdating = false;
  @override
  bool get alreadyUpdating => _alreadyUpdating;
  @override
  set alreadyUpdating (bool val)=> _alreadyUpdating = val;

  @override
  Future<Identity> update() async {
    if(!alreadyUpdating) {
      alreadyUpdating = true;
      await repo.getOwnIdentityDetails().then((ownId) => _add(ownId));
      alreadyUpdating = false;
    }
    return state;
  }
}