## To build GitlabCI base image
# export CI_IMAGE_NAME="registry.gitlab.com/elrepo.io/elrepo.io-android:android_base"
# docker build --squash -t "${CI_IMAGE_NAME}" -f misc/android_base.Dockerfile .
# docker push "${CI_IMAGE_NAME}"

## To extract the generated APK easily you can run after the build complete
# docker cp \
#	$(docker create --rm ${CI_IMAGE_NAME}):${APK_OUTPUT_DIR}/ \
#	/tmp/${APK_OUTPUT_DIR}/

FROM cirrusci/flutter:latest

ENV B_WORKDIR="/elRepo.io-android/"
ARG SOURCE_PATH="./"
COPY "$SOURCE_PATH/" "$B_WORKDIR"

ENV APK_OUTPUT_DIR="/elRepo.io-APK/"

WORKDIR "$B_WORKDIR"
RUN flutter packages upgrade && flutter build apk && \
	cp --recursive build/app/outputs/flutter-apk/ "$APK_OUTPUT_DIR"

WORKDIR /
RUN rm -rf "$B_WORKDIR" 

# if everything goes fine the APK will be available at
# "$APK_OUTPUT_DIR/app-release.apk"
