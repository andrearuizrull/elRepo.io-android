/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import 'dart:async';

import 'package:elRepoIo/ui/circles/circle_card.dart';
import 'package:elRepoIo/ui/common/no_data_yet.dart';
import 'package:elRepoIo/ui/common/updatable_future_builder.dart';
import 'package:elRepoIo/ui/common/updatable_list_page.dart';
import 'package:elRepoIo/ui/providers/circles_provider.dart';
import 'package:flutter/material.dart';
import 'package:elrepo_lib/repo.dart' as repo;

import 'package:elRepoIo/constants.dart';
import 'package:elRepoIo/generated/l10n.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class CircleScreen extends ConsumerStatefulWidget {
  @override
  _CircleScreenState createState() => _CircleScreenState();
}

class _CircleScreenState extends ConsumerState<CircleScreen>
    with TickerProviderStateMixin {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.pushNamed(context, '/circleDetails');
        },
        backgroundColor: REMOTE_COLOR,
        child: Icon(Icons.add),
      ),
      appBar: AppBar(
        backgroundColor: PURPLE_COLOR.withOpacity(0.9),
        shadowColor: REMOTE_COLOR,
        brightness: Brightness.dark,
        elevation: 0,
        leading: IconButton(
          onPressed: () {
            Navigator.of(context).pop();
          },
          icon: Icon(
            Icons.arrow_back_rounded,
            color: WHITE_COLOR,
          ),
        ),
        title: Text(
          S.of(context).circlesTitle,
          style: TextStyle(fontWeight: FontWeight.w800, color: WHITE_COLOR),
        ),
      ),
      body: UpdatableListPage(
        circlesProvider,
        callbackWidget: _circleCard,
        timerCallback: _autoUpdateCb,
        noDataWidget: (ref) => circlesNoData(),
      ),
    );
  }

  Widget _circleCard(dynamic data) => CircleCard(data);

  void _autoUpdateCb(WidgetRef ref, Timer timer) =>
    ref.watch(circlesProvider.notifier).update();

}
