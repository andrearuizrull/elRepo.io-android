/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import 'dart:async';
import 'dart:io';

import 'package:archive/archive_io.dart';
import 'package:elRepoIo/RsServiceControl/rs_folders_control.dart';
import 'package:elRepoIo/RsServiceControl/rscontrol.dart';
import 'package:elRepoIo/routes.dart';
import 'package:elRepoIo/ui/common/loading_dialog.dart';
import 'package:elRepoIo/ui/login.dart';
import 'package:elRepoIo/ui/providers/timers_provider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:path_provider/path_provider.dart';
import 'package:retroshare_dart_wrapper/retroshare.dart' as rs;
import 'package:elRepoIo/RsServiceControl/rscontrol.dart' as rscontrol;
import 'package:share_plus/share_plus.dart';
import 'package:path/path.dart' as path;
import 'package:archive/archive.dart';

import 'package:elRepoIo/constants.dart';
import 'package:elRepoIo/generated/l10n.dart';
import 'package:elRepoIo/ui/common/ui_utils.dart';

class CreateBackupScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BackupDownloading();
  }
}

class BackupDownloading extends ConsumerStatefulWidget {
  @override
  _BackupDownloadingState createState() => _BackupDownloadingState();
}

class _BackupDownloadingState extends ConsumerState<BackupDownloading> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: PURPLE_COLOR.withOpacity(0.9),
          shadowColor: REMOTE_COLOR,
          brightness: Brightness.dark,
          elevation: 0,
          iconTheme: IconThemeData(color: Colors.black),
          leading: IconButton(
            onPressed: () {
              Navigator.of(context).pop();
            },
            icon: Icon(
              Icons.arrow_back_rounded,
              color: WHITE_COLOR,
            ),
          ),
          title: Text(
            'Create a Backup',
            style: TextStyle(
              fontWeight: FontWeight.w800,
              color: WHITE_COLOR,
            ),
          ),
        ),
        body: Container(
            width: double.infinity,
            padding: EdgeInsets.symmetric(vertical: 14),
            child: Center(
              child: ListView(
                shrinkWrap: true,
                padding: EdgeInsets.all(15.0),
                children: [
                  Padding(
                    padding: EdgeInsets.symmetric(
                      horizontal: 32,
                      vertical: 12,
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          S.of(context).backupText,
                          style: TextStyle(
                            color: REMOTE_COLOR,
                          ),
                        ),
                        SizedBox(
                          height: MediaQuery.of(context).size.height * .02,
                        ),
                        Divider(
                          height: MediaQuery.of(context).size.height * 0.004,
                          color: Colors.grey,
                        ),
                        SizedBox(
                          height: MediaQuery.of(context).size.height * .02,
                        ),
                        RaisedButton(
                          color: LOCAL_COLOR,
                          disabledColor: ACCENT_COLOR,
                          elevation: 4.0,
                          splashColor: REMOTE_COLOR,
                          onPressed: () async {
                            final sharedDirs = await rs.RsFiles.getSharedDirectories();
                            if (sharedDirs.isNotEmpty) {
                              // Await RS is stopped
                              var pr = ProgressDialog(context,
                                  initialMessage: S.of(context).creatingBackup)
                                ..show();
                              ref.read(timersProvider.notifier).cancelAll(); // Cancel all global timers
                              await Future.delayed(Duration(seconds: 1)); // Await all remaining petitions stop
                              await rscontrol.RsServiceControlPlatform.instance.stopRetroShareExecution();

                              // Start zip creation
                              final backupZipPath = await RsBackup.createZipBackup(sharedDirs);
                              Share.shareFiles([backupZipPath],
                                  text: 'elRepo.io backup zip file').then((value) async { // todo(intl)
                                rscontrol.RsServiceControlPlatform
                                    .instance.startRetroshareLoop().then((value) {
                                      // todo: Do autologin after backup
                                      // How we have to restart all disposed timers?
                                      pr.hide();
                                      popUntilLogin(context);
                                });
                              });

                            }
                          },
                          textColor: WHITE_COLOR,
                          child: Text(
                            S.of(context).createandshare,
                            textAlign: TextAlign.center,
                          ),
                        ),
                        SizedBox(height: 20,),
                        Row(
                            children:[
                              Icon(Icons.warning, color: RED_COLOR,),
                              SizedBox(width: 20,),
                              Expanded(
                                  child: Text('Alert! Downloaded or shared files are not included on this backup!\n'
                                      'Backup it manually', style: TextStyle(color: REMOTE_COLOR),)
                              ),
                        ]),
                      ],
                    ),
                  ),
                ],
              ),
            )),
      ),
    );
  }
}
