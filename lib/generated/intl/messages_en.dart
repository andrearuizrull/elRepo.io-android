// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a en locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'en';

  static m0(url) => "Connetected to: ${url}";

  static m1(number) => "${number}% match";

  static m2(number) => "Progress: ${number} %";

  static m3(rsPrefix) => "Connected to: ${rsPrefix}";

  final messages = _notInlinedMessages(_notInlinedMessages);
  static _notInlinedMessages(_) => <String, Function> {
    "addFriendButton" : MessageLookupByLibrary.simpleMessage("Add friend"),
    "addFriendHint" : MessageLookupByLibrary.simpleMessage("Paste the invite here"),
    "addFriendInstructions" : MessageLookupByLibrary.simpleMessage("To manually add friends and connect to them on elRepo.io network, you need to do two actions:\n1. Share your elRepo.io id with your friends \n2. Get your friends ids and add it to your elRepo.io network\nRemember that both actions have to be done in order to become friends!"),
    "addFriendPeerIdExplanation" : MessageLookupByLibrary.simpleMessage("Save your friends to your elRepo.io network"),
    "addFriendPeerIdTitle" : MessageLookupByLibrary.simpleMessage("Add your friend\'s invite"),
    "addFriendYourIdExplanation" : MessageLookupByLibrary.simpleMessage("Send your elRepo.io invite to your friends so they can add you to their elRepo.io network"),
    "addFriendYourIdTitle" : MessageLookupByLibrary.simpleMessage("Your information"),
    "appIntro" : MessageLookupByLibrary.simpleMessage("App intro"),
    "appSettings" : MessageLookupByLibrary.simpleMessage("App settings"),
    "attemptconnection" : MessageLookupByLibrary.simpleMessage("Attempt connection"),
    "automaticpeering" : MessageLookupByLibrary.simpleMessage("Automatic peering"),
    "backup" : MessageLookupByLibrary.simpleMessage("Create a Backup"),
    "backupText" : MessageLookupByLibrary.simpleMessage("Your account data will be copied to a zip file and you will be presented with a menu to share it.\n\n Choose a destination you consider secure and reliable.\n\nOnce the backup is shared you will need to log back in to elRepo.io\n\nIf you ever need to restore this backup, you will need to download the zip file\n\n and provide it in the sign-up screen."),
    "bookmarkContent" : MessageLookupByLibrary.simpleMessage("Downloading content..."),
    "browse" : MessageLookupByLibrary.simpleMessage("Browse"),
    "cancel" : MessageLookupByLibrary.simpleMessage("Cancel"),
    "cannotLoadContent" : MessageLookupByLibrary.simpleMessage("Cannot load content"),
    "checkItOut" : MessageLookupByLibrary.simpleMessage("Check it out"),
    "chooseFile" : MessageLookupByLibrary.simpleMessage("Choose File"),
    "circle" : MessageLookupByLibrary.simpleMessage("Circle"),
    "circleAcceptInvite" : MessageLookupByLibrary.simpleMessage("Accept Invite"),
    "circleCardContent" : MessageLookupByLibrary.simpleMessage("Leave or join a circle will update 20 or 30 seconds after accept or revoke the invite."),
    "circleCardTitle" : MessageLookupByLibrary.simpleMessage("Information"),
    "circleDetailsCreateCircle" : MessageLookupByLibrary.simpleMessage("Creating circle"),
    "circleDetailsFinishing" : MessageLookupByLibrary.simpleMessage("Finishing"),
    "circleDetailsUpdateCircle" : MessageLookupByLibrary.simpleMessage("Updating circle details"),
    "circleErrorNameEmpty" : MessageLookupByLibrary.simpleMessage("Circle name can\'t be empty."),
    "circleErrorNoContactsYet" : MessageLookupByLibrary.simpleMessage("No contacts yet, wait for propagation."),
    "circleErrorNoIds" : MessageLookupByLibrary.simpleMessage("No ids selected"),
    "circleErrorSelectPerson" : MessageLookupByLibrary.simpleMessage("You have to select at least one person to add to the circle."),
    "circleInvitePeople" : MessageLookupByLibrary.simpleMessage("and invite this people?"),
    "circleSelectCircle" : MessageLookupByLibrary.simpleMessage("Select a circle"),
    "circleSelectPeople" : MessageLookupByLibrary.simpleMessage("Select people to invite to your circle."),
    "circleWithMemberList" : MessageLookupByLibrary.simpleMessage("with this membership list?"),
    "circlesTitle" : MessageLookupByLibrary.simpleMessage("Circles"),
    "close" : MessageLookupByLibrary.simpleMessage("Close"),
    "comment" : MessageLookupByLibrary.simpleMessage("Comment"),
    "commentPosted" : MessageLookupByLibrary.simpleMessage("Comment posted"),
    "commonButtonsSynchronizePosts" : MessageLookupByLibrary.simpleMessage("Synchronizing posts"),
    "connect" : MessageLookupByLibrary.simpleMessage("Connect"),
    "cont" : MessageLookupByLibrary.simpleMessage("Continue"),
    "contentPublished" : MessageLookupByLibrary.simpleMessage("Content published"),
    "create" : MessageLookupByLibrary.simpleMessage("Create"),
    "createAccount" : MessageLookupByLibrary.simpleMessage("Create account"),
    "createandshare" : MessageLookupByLibrary.simpleMessage("Create and share backup file"),
    "creatingAccount" : MessageLookupByLibrary.simpleMessage("... creating account.\n This may take a while."),
    "creatingBackup" : MessageLookupByLibrary.simpleMessage("Creating backup, please wait"),
    "darkMode" : MessageLookupByLibrary.simpleMessage("Dark Theme"),
    "delete" : MessageLookupByLibrary.simpleMessage("Delete"),
    "description" : MessageLookupByLibrary.simpleMessage("Description"),
    "details" : MessageLookupByLibrary.simpleMessage("Details"),
    "disconnect" : MessageLookupByLibrary.simpleMessage("Disconnect"),
    "discover" : MessageLookupByLibrary.simpleMessage("discover"),
    "edit" : MessageLookupByLibrary.simpleMessage("Edit"),
    "elRepoIoIsLoading" : MessageLookupByLibrary.simpleMessage("elRepo.io is Loading ..."),
    "elRepoio" : MessageLookupByLibrary.simpleMessage("elRepo.io"),
    "error" : MessageLookupByLibrary.simpleMessage("Error"),
    "fileDownloadCardCancelDownload" : MessageLookupByLibrary.simpleMessage("Cancel download"),
    "fileDownloadCardCancelDownloadConfirmation" : MessageLookupByLibrary.simpleMessage("Are you sure that you want to cancel this download?"),
    "fileDownloadCardHashing" : MessageLookupByLibrary.simpleMessage("Hashing..."),
    "fileDownloadCardLoadingInfo" : MessageLookupByLibrary.simpleMessage("Loading file info..."),
    "fileDownloadDeleteFile" : MessageLookupByLibrary.simpleMessage("Delete file"),
    "fileDownloadDeleteFileConfirmation" : MessageLookupByLibrary.simpleMessage("This action will delete the file from your file  system. Are you sure to do that?"),
    "follow" : MessageLookupByLibrary.simpleMessage("follow"),
    "following" : MessageLookupByLibrary.simpleMessage("following"),
    "friendNodesConnected" : m0,
    "friendNodesTitle" : MessageLookupByLibrary.simpleMessage("elRepo.io network"),
    "haveABackup" : MessageLookupByLibrary.simpleMessage("I have a backup"),
    "knownnodes" : MessageLookupByLibrary.simpleMessage("Known nodes"),
    "language" : MessageLookupByLibrary.simpleMessage("Choose a language"),
    "languageDropdownHint" : MessageLookupByLibrary.simpleMessage("Publication languages"),
    "languageDropdownNothingSelected" : MessageLookupByLibrary.simpleMessage("Nothing selected"),
    "leave" : MessageLookupByLibrary.simpleMessage("Leave"),
    "localRepo" : MessageLookupByLibrary.simpleMessage("local\nrepo"),
    "loggingIn" : MessageLookupByLibrary.simpleMessage("Logging in. This can take a while..."),
    "loggingInAs" : MessageLookupByLibrary.simpleMessage("Logging as"),
    "login" : MessageLookupByLibrary.simpleMessage("Log in"),
    "loginFailed" : MessageLookupByLibrary.simpleMessage("Login failed. Please retry."),
    "matchPercent" : m1,
    "name" : MessageLookupByLibrary.simpleMessage("Name"),
    "network" : MessageLookupByLibrary.simpleMessage("Your elRepo.io network"),
    "noResults" : MessageLookupByLibrary.simpleMessage("No results"),
    "notifications" : MessageLookupByLibrary.simpleMessage("Receive notifications"),
    "ok" : MessageLookupByLibrary.simpleMessage("ok"),
    "onboardAvoidCensorship" : MessageLookupByLibrary.simpleMessage("Avoid censorship"),
    "onboardAvoidCensorshipText" : MessageLookupByLibrary.simpleMessage("Share your content without depending on information silos controlled by global monopolies"),
    "onboardDownCloud" : MessageLookupByLibrary.simpleMessage("bring down the cloud"),
    "onboardFamily" : MessageLookupByLibrary.simpleMessage("Welcome to elRepo.io \"glocal\" family!"),
    "onboardFamilyText" : MessageLookupByLibrary.simpleMessage("You can now begin sharing and exploring content."),
    "onboardLocalGlobal" : MessageLookupByLibrary.simpleMessage("Local, global, online and offline"),
    "onboardLocalGlobalText" : MessageLookupByLibrary.simpleMessage("Publish and view content even when you are not connected to the Internet."),
    "onboardPopBubble" : MessageLookupByLibrary.simpleMessage("Pop the information bubble"),
    "onboardPopBubbleText" : MessageLookupByLibrary.simpleMessage("What you search is what you get. No intrusive AI or algorithms will get between you and content shared by others"),
    "participants" : MessageLookupByLibrary.simpleMessage("Participants"),
    "password" : MessageLookupByLibrary.simpleMessage("Password"),
    "passwordCannotBeEmpty" : MessageLookupByLibrary.simpleMessage("Password cannot be empty."),
    "peeringdescription" : MessageLookupByLibrary.simpleMessage("If activated you will automatically connect with other elRepo.io nodes in your local network."),
    "postingComment" : MessageLookupByLibrary.simpleMessage("posting comment"),
    "progresPercent" : m2,
    "publish" : MessageLookupByLibrary.simpleMessage("Publish"),
    "publishingContent" : MessageLookupByLibrary.simpleMessage("Publishing content...\n If you have added heavy files this may take several minutes to complete."),
    "remoteControl" : MessageLookupByLibrary.simpleMessage("Remote Control"),
    "remoteControlConnectedTo" : m3,
    "remoteControlLocallyConnected" : MessageLookupByLibrary.simpleMessage("not connected to a remote elRepo.io node"),
    "remoteControlerror" : MessageLookupByLibrary.simpleMessage("ERROR: Could not connect to remote node!"),
    "remoteCtrl" : MessageLookupByLibrary.simpleMessage("Remote Control"),
    "remoteRules" : MessageLookupByLibrary.simpleMessage("You need to specify the complete URL to your remote node control API."),
    "restoreBackup" : MessageLookupByLibrary.simpleMessage("Restoring backup"),
    "save" : MessageLookupByLibrary.simpleMessage("Save"),
    "savingContentToLocalRepo" : MessageLookupByLibrary.simpleMessage("Downloading content..."),
    "search" : MessageLookupByLibrary.simpleMessage("Search"),
    "searchOverRepoio" : MessageLookupByLibrary.simpleMessage("Searching over elrepo.io network"),
    "searchSimilar" : MessageLookupByLibrary.simpleMessage("Search similar"),
    "settings" : MessageLookupByLibrary.simpleMessage("Settings"),
    "signOut" : MessageLookupByLibrary.simpleMessage("SIGN OUT"),
    "signUp" : MessageLookupByLibrary.simpleMessage("Sign up"),
    "tabsDiscover" : MessageLookupByLibrary.simpleMessage("Discover"),
    "tabsDownloaded" : MessageLookupByLibrary.simpleMessage("Downloaded"),
    "tabsExplore" : MessageLookupByLibrary.simpleMessage("Explore"),
    "tabsFollowing" : MessageLookupByLibrary.simpleMessage("Following"),
    "tabsInProgress" : MessageLookupByLibrary.simpleMessage("In Progress"),
    "tabsLocal" : MessageLookupByLibrary.simpleMessage("Local"),
    "tabsTags" : MessageLookupByLibrary.simpleMessage("Tags"),
    "tabsYourContent" : MessageLookupByLibrary.simpleMessage("Your content"),
    "tags" : MessageLookupByLibrary.simpleMessage("tags"),
    "title" : MessageLookupByLibrary.simpleMessage("title"),
    "titleCannotBeEmpty" : MessageLookupByLibrary.simpleMessage("Title cannot be empty."),
    "username" : MessageLookupByLibrary.simpleMessage("Username"),
    "usernameCannotBeEmpty" : MessageLookupByLibrary.simpleMessage("Username cannot be empty."),
    "yes" : MessageLookupByLibrary.simpleMessage("Yes"),
    "youCanAddHashTags" : MessageLookupByLibrary.simpleMessage("Tip: you can add #HashTags in your description to make it easier to discover your post."),
    "your" : MessageLookupByLibrary.simpleMessage("Your")
  };
}
