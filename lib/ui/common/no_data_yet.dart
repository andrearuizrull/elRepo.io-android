/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import 'package:elRepoIo/constants.dart';
import 'package:elRepoIo/generated/l10n.dart';
import 'package:elRepoIo/ui/main_page/models/commons.dart';
import 'package:elRepoIo/ui/main_page/models/tab_navigation_item.dart' show PUBLISH_ICON, DISCOVER_ICON;
import 'package:elRepoIo/ui/common/connection_status.dart';
import 'package:elRepoIo/ui/common/identity_card.dart';
import 'package:elRepoIo/ui/common/ui_utils.dart';
import 'package:elRepoIo/ui/providers/identities_provider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:retroshare_dart_wrapper/rs_models.dart';

/// Used to show all no data yet on a no data yet page for the documentation
var noDataYetArray = [
  circlesNoData(),
  yourContentNoData(),
  downloadedNoData(),
  exploreNoData(),
  followingNoData(),
];

// todo(intl) all this page

/// Return a [NoDataYet] for 'Circles' screen
NoDataYet circlesNoData () => NoDataYet(
  'You don\'t belong to any circle',
  'Circles will let you to share publication privately with a group of users',
  image: Image.asset(
    'assets/circles.png',
    width: 100,
  ),
  leading: Icon(Icons.feedback),
  expansion: Column(
    mainAxisAlignment: MainAxisAlignment.start,
    crossAxisAlignment: CrossAxisAlignment.start,
    mainAxisSize: MainAxisSize.max,
    children: [
      Text('To create a circle use the floating button below. If you are invited to a '
          'circle you will see the invite on this screen.'),
      SizedBox(height: 15,),
      Text('Circles need to be propagated through the elRepo.io network, so you may wait some '
          'minutes before see an invite. Then, you will see all the content published under this'
          'circle as any normal content on explore screen.'),
      SizedBox(height: 15,),
      Text('On the create circle screen you can select the users with who you want to share it, '
          'for more security you can find a user using it unique id key, that you will find under '
          'settings screen. \n This is your id, use a long press to copy it and share it:'),
      OwnIdentityCard(),
    ],),
);

/// Return a [NoDataYet] for 'your content' screen
NoDataYet yourContentNoData () => NoDataYet(
  'Ready to publish?',
  'This page will show your own publications, but is empty...',
  image: Image.asset(
    'assets/message.png',
    width: 100,
  ),
  leading: Icon(Icons.feedback),
  expansion: Column(
    mainAxisAlignment: MainAxisAlignment.start,
    crossAxisAlignment: CrossAxisAlignment.start,
    mainAxisSize: MainAxisSize.max,
    children: [
      Text('To publish click on the bottom icon to go to the publish screen:'),
      SizedBox(height: 15,),
      Center(
          child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: BottomButtonCard(PUBLISH_ICON, S.current.publish)
          )
      ),
      Text('There you can publish what you want! Remember to be connected to somebody to start share your publications.'),
      RichText(
        text: TextSpan(
            text: 'You can share files or not, and use ',
            style: TextStyle(color: Colors.black),
            children:[
              TextSpan(text: '#HashTags', style: TextStyle(color: REMOTE_COLOR)),
              TextSpan(text: ' to make your publication more visible.', style: TextStyle(color:  Colors.black)),
            ]) ,
      ),
    ],),
);

/// Return a [NoDataYet] for 'Downloaded' screen
NoDataYet downloadedNoData ({downloading = false}) => NoDataYet(
  !downloading ? 'Nothing downloaded!' : 'Not downloading yet!',
  'Explore elRepo.io network to find content to download',
  image: Image.asset(
    'assets/search.png',
    width: 100,
  ),
  leading: Icon(Icons.feedback),
  expansion: Column(
    mainAxisAlignment: MainAxisAlignment.start,
    crossAxisAlignment: CrossAxisAlignment.start,
    mainAxisSize: MainAxisSize.max,
    children: [
      Text('On the bottom menu look at Discover tab:'),
      SizedBox(height: 15,),
      Center(
          child: BottomButtonCard(DISCOVER_ICON, S.current.tabsDiscover)
      ),
      Text('There you will find publications from other peers on the network to download them.'),
    ],),
);

/// Return a [NoDataYet] for 'Downloaded' screen
NoDataYet exploreNoData () => NoDataYet(
  'Arranging data from elRepo.io network',
  'P2P connections in progress',
  image: Image.asset(
    'assets/cloud.png',
    width: 100,
  ),
  leading: Icon(Icons.feedback),
  expansion: Column(
    mainAxisAlignment: MainAxisAlignment.start,
    crossAxisAlignment: CrossAxisAlignment.start,
    mainAxisSize: MainAxisSize.max,
    children: [
      Text('ElRepo.io is a decentralized friend to friend network, in opposition to '
          'other centralized apps, the data will be downloaded to your device. If this '
          'process is to long, take a look at your connection status, on the top bar icon:'),
      SizedBox(height: 15,),
      Center(
          child: ConnectionStatusIcon()
      ),
      Text('This icon could help you to check if you are connected to the netowrk or not.'),
      Text('ElRepo.io works without going out to internet, also in your local network! Share '
          'with your friends without the control of big corporations.'),
    ],),
);


/// Return a [NoDataYet] for 'Downloaded' screen
NoDataYet followingNoData () => NoDataYet(
  'Following anybody!',
  'Don\'t lose anything from authors you like!',
  image: Image.asset(
    'assets/flowchart.png',
    width: 100,
  ),
  leading: Icon(Icons.feedback),
  expansion: Column(
    mainAxisAlignment: MainAxisAlignment.start,
    crossAxisAlignment: CrossAxisAlignment.start,
    mainAxisSize: MainAxisSize.max,
    children: [
      Text('To follow an author just access to their profile taping their name '
          'from a publication details, then, click the follow button to start following!'),
      SizedBox(height: 15,),
      Center(
          child: Consumer(
            builder: (BuildContext context, WidgetRef ref, Widget child) {
              return IdentityCardLittle(ref.watch(ownIdentityProvider));
            },
          )
      ),
    ],),
);

/// Widget that emulates the bottom menu buttons to change the tabs. Used on the
/// descriptions
class BottomButtonCard extends StatelessWidget {
  final IconData icon;
  final String text;
  const BottomButtonCard(this.icon, this.text, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 1.0,
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
            children: [
              Icon(icon, color: Colors.grey,),
              Text(text, style: TextStyle(color: Colors.grey, fontSize: 12.0),),
            ]),
      ),
    );
  }
}

/// Default widget that will show when provider is still empty.
///
class NoDataYet extends StatelessWidget {
  /// Leading image
  final Image image;
  final String title, subtitle;
  final Widget leading,
  /// Widget that will be show when expansion panel is open
      expansion;
  const NoDataYet(
      this.title,
      this.subtitle,
      {this.image,
        this.expansion,
        this.leading,
        Key key,}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
      elevation: 1.0,
      clipBehavior: Clip.antiAlias,
      child: Column(
        children: [
          SizedBox(height: 15,),
          if(image != null) image, //Image.asset('assets/cloud.png')
          ExpansionTile(
            leading: leading,
            title: Text(title),
            subtitle: Text(
              subtitle,
              style: TextStyle(color: Colors.black.withOpacity(0.6)),
            ),
            children: [
              Padding(
                padding: const EdgeInsets.all(16.0),
                child: expansion,
                // child: Text(
                //   'Greyhound divisively hello coldly wonderfully marginally far upon excluding.',
                //   style: TextStyle(color: Colors.black.withOpacity(0.6)),
                // ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}

/// Page to show all the no data yet info and go to onboarding screen
class NoDataYetPage extends StatelessWidget {
  const NoDataYetPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: PURPLE_COLOR.withOpacity(0.9),
          shadowColor: REMOTE_COLOR,
          brightness: Brightness.dark,
          elevation: 0,
          //iconTheme: IconThemeData(color: Colors.black),
          leading: IconButton(
            onPressed: () {
              Navigator.of(context).pop();
            },
            icon: Icon(
              Icons.arrow_back_rounded,
              color: WHITE_COLOR,
            ),
          ),
          title: Text(
            S.of(context).settings,
            style: TextStyle(
              fontWeight: FontWeight.w800,
              color: WHITE_COLOR,
            ),
          ),
        ),
        body: Container(
            decoration: defaultBackground,
            child: ListView(
                children: [
                  Card(
                    margin: EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
                    elevation: 1.0,
                    clipBehavior: Clip.antiAlias,
                    child: ListTile(
                      leading: Icon(
                        Icons.privacy_tip,
                        color: LOCAL_COLOR,
                      ),
                      title: Text(S.of(context).appIntro),
                      trailing: Icon(
                        Icons.keyboard_arrow_right_rounded,
                        color: LOCAL_COLOR,
                      ),
                      onTap: () {
                        Navigator.pushNamed(context, '/onboarding');
                      },
                    ),
                  ),
                  buildDivider(),
                  ...noDataYetArray,
                ])
        ));
  }
}

