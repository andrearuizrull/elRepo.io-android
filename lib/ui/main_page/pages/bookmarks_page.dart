/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import 'package:elRepoIo/ui/main_page/models/browse_tab_bar.dart';
import 'package:elRepoIo/ui/main_page/models/commons.dart';
import 'package:elRepoIo/ui/common/no_data_yet.dart';
import 'package:elRepoIo/ui/common/updatable_list_page.dart';
import 'package:elRepoIo/generated/l10n.dart';
import 'package:elRepoIo/ui/common/posts_teaser.dart';
import 'package:elRepoIo/ui/providers/teasers_providers.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:retroshare_dart_wrapper/rs_models.dart';

class BookmarksScreen extends ConsumerStatefulWidget {
  static Route<dynamic> route() => MaterialPageRoute(
    builder: (context) => BookmarksScreen(),
  );

  @override
  _BookmarksScreenState createState() => _BookmarksScreenState();
}

class _BookmarksScreenState extends ConsumerState<BookmarksScreen> {

  // todo(kon): this callback only starts when downloaded screen is shown.
  // Instead, the callback on explore screen is called from the startup...
  // IDK why. This is the expected behaviour.
  void updateDownloadedCallback(ref, timer){
    if(ref.read(allBookmarksProvider).isNotEmpty) {
      ref.watch(allBookmarksProvider.notifier).update();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BrowseXScreen(myTabs: {
        S.current.tabsYourContent: UpdatableListPage<RsMsgMetaData>(
          userPostsProvider,
          backgroundDecoration: defaultBackground,
          noDataWidget: (ref) => yourContentNoData(),
          // callbackWidget: (t) => PostTeaserCard(t),
          callbackWidget: (data) => PostTeaserCard(data,),
        ),
        S.current.tabsDownloaded: UpdatableListPage<RsMsgMetaData>(
            allBookmarksProvider,
            backgroundDecoration: defaultBackground,
            noDataWidget: (ref) => downloadedNoData(),
            listProvider: downloadedProvider,
            callbackWidget: (data) => PostTeaserCard(data,),
            timerCallback: updateDownloadedCallback
        ),
        S.current.tabsInProgress: UpdatableListPage<RsMsgMetaData>(
            allBookmarksProvider,
            backgroundDecoration: defaultBackground,
            noDataWidget: (ref) => downloadedNoData(downloading: true),
            listProvider: downloadingProvider,
            callbackWidget: (data) => PostTeaserCard(data,),
            timerCallback: updateDownloadedCallback
        ),
      }),
    );
  }
}
