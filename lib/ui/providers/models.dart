/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import 'package:flutter_riverpod/flutter_riverpod.dart';

/// Implements interface of an [StateNotifier] with an `update` function
abstract class UpdatableNotifier<T> extends StateNotifier<T>{
  /// Implement this to check when the update function is executing to don't
  /// perform the update multiple times causing bugs.
  bool get alreadyUpdating;
  set alreadyUpdating(bool val);

  UpdatableNotifier(state) : super(state);
  Future<void> update();
}

/// Interface that implement lazy updates for notifiers
///
/// Basically the lazy update will make a copy of the state with the same thata
/// that will force a rebuild of widgets.
abstract class LazyUpdateNotifier<T> extends StateNotifier<T> {
  LazyUpdateNotifier(state) : super(state);
  void lazyUpdate();
}