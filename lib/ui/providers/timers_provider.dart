/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */


import 'dart:async';
import 'dart:math';

import 'package:flutter_riverpod/flutter_riverpod.dart';

/// Provider to store global timers
///
/// If the timers live globally, you don't have to handle a timer that is runing
/// from a dead widget, or else, you can cancel all timers when close the
/// session, and don't be worried by the widget management, that causes bugs.
final timersProvider  = StateNotifierProvider<TimersNotifier, Map<String, Timer>>((ref) {
  return TimersNotifier();
});

class TimersNotifier extends StateNotifier<Map<String, Timer>> {
  TimersNotifier( ): super(<String, Timer>{});

  /// Used to simply create unique id for timers without external package
  ///
  /// Called when a timer is stored without key
  // todo: just for check, is this the best way to solve this?
  String _generateRandomUid() =>
    (Random().nextInt(100) + DateTime.now().millisecondsSinceEpoch + Random().nextInt(1000)).toString();

  Timer addTimer(Timer t, [String key]) {
    key = key ?? _generateRandomUid();
    state = {...state, key: t};
    return state[key];
  }

  void cancelTimer(String key) {
    state[key].cancel();
    state = state..remove(state[key]);
  }

  void cancelAll() {
    state.forEach((key, timer) => timer.cancel());
    state = <String, Timer>{};
  }

  @override
  void dispose() {
    cancelAll();
    super.dispose();
  }

}
