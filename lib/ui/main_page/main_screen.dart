/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import 'package:elRepoIo/routes.dart' show TabsPageArguments;
import 'package:elRepoIo/constants.dart';
import 'package:elRepoIo/ui/common/connection_status.dart';
import 'package:elRepoIo/ui/common/ui_utils.dart';
import 'package:elRepoIo/ui/main_page/pages/bottom_tabs.dart';
import 'package:flutter/material.dart';

class MainScreen extends StatelessWidget {

  /// Used to receive intents and set opened tab
  TabsPageArguments tabsPageArguments;

  MainScreen({this.tabsPageArguments});

  int _showVersion =
      0; // Easter egg that show version of RetroShare and elrepo.io

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          centerTitle: false,
          backgroundColor: PURPLE_COLOR.withOpacity(0.9),
          shadowColor: REMOTE_COLOR,
          brightness: Brightness.dark,
          toolbarOpacity: 1.0,
          leading: GestureDetector(
            onTap: () {
              ++_showVersion;
              print('Show version dialog in $_showVersion');
              if (_showVersion % 5 == 0) showVersionDialog(context);
            },
            child: Image.asset("assets/logo.png"),
          ),
          title: Text(
            'elRepo.io',
            style: TextStyle(color: WHITE_COLOR),
          ),
          elevation: 3,
          actions: <Widget>[
            ConnectionStatusIcon(),
            IconButton(
              icon: Icon(
                Icons.search,
                color: WHITE_COLOR,
              ),
              onPressed: () {
                Navigator.pushNamed(context, '/search');
              },
            ),
            IconButton(
              icon: Icon(
                Icons.settings,
                color: WHITE_COLOR,
              ),
              onPressed: () {
                Navigator.pushNamed(context, '/settings');
              },
            ),
          ],
        ),
        body: TabsPage(tabsPageArguments: tabsPageArguments),
      ),
    );
  }
}
