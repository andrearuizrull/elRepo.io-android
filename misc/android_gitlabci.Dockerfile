FROM registry.gitlab.com/elrepo.io/elrepo.io-android:android_base

ENV B_WORKDIR="/elRepo.io-android/"
ARG SOURCE_PATH="./"
COPY "$SOURCE_PATH/" "$B_WORKDIR"

ENV APK_OUTPUT_DIR="/elRepo.io-APK/"

WORKDIR "$B_WORKDIR"
RUN rm -rf "$APK_OUTPUT_DIR" && \
	flutter packages upgrade && flutter build apk && \
	cp --recursive build/app/outputs/flutter-apk/ "$APK_OUTPUT_DIR"

# if everything goes fine the APK will be available at
# "$APK_OUTPUT_DIR/app-release.apk"
