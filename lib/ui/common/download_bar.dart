/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import 'dart:async';

import 'package:elrepo_lib/repo.dart';
import 'package:flutter/material.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';
import 'package:retroshare_dart_wrapper/retroshare.dart' as rs;
import 'package:elrepo_lib/repo.dart' as repo;
import 'package:elRepoIo/generated/l10n.dart';

import '../../constants.dart';

/// Download bar to manage file download by hash
///
/// At 100% it passes to the [onDone] callback the path String. It could be null
/// in case RS is still hashing the downloaded file.
///
/// Uses [LinearPercentIndicator] for the download and change to indeterminate
/// [LinearProgressIndicator] when hashing.
class DownloadBar extends StatefulWidget {
  final String downloadingHash;
  final Function(String) onDone;

  const DownloadBar({Key key, @required this.downloadingHash, this.onDone})
      : super(key: key);

  @override
  _DownloadBarState createState() => _DownloadBarState();
}

class _DownloadBarState extends State<DownloadBar> {
  String _downloadingHash;
  Timer _downloadProgressTimer;
  double downloadProgress = 0.0;
  Future<String> filePath;
  bool fileExists = false;
  double _progress;
  bool isHashing =
      false; // At the end of download RS take a while hashing the file, this handle this state

  @override
  void initState() {
    _downloadingHash = widget.downloadingHash;
    startDownloadProgressTimer();
    super.initState();
  }

  @override
  void dispose() {
    _downloadProgressTimer?.cancel();
    super.dispose();
  }

  // todo(kon): move all this logic to elrepo-lib somehow
  Future<void> startDownloadProgressTimer() async {
    var hashes = await rs.RsFiles.fileDownloads();
    if (hashes.isNotEmpty) {
      for (var hash in hashes) {
        if (hash == _downloadingHash) {
          _progress = 0.0;
          _downloadProgressTimer = Timer.periodic(
            Duration(seconds: 1),
            _downloadProgressHandler,
          );
        }
      }
    }
  }

  /// Called once download could be finished
  void _checkDownloadFinished(Timer timer) {
    // It takes a while for the downloaded file to be hashed by Retroshare
    // so we keep the timer up until it's done
    filePath = repo.getPathIfExists(_downloadingHash);
    filePath.then((path) async {
      // We pass the path to the callback to let the parent widget manage
      // the wait time until check hash is finish.
      widget.onDone(path);
      // The file is already available on this device
      if (path != null) {
        timer.cancel();
      } else if (path == null && !isHashing && mounted) {
        setState(() => isHashing = true);
      }
    });
  }

  void _downloadProgressHandler(Timer timer) async {
    var downloadDetails = await rs.RsFiles.fileDetails(_downloadingHash);
    _progress =
        downloadDetails['avail']['xint64'] / downloadDetails['size']['xint64'];
    print('progress for $_downloadingHash: $_progress');

    if (timer.isActive) {
      if (mounted) setState(() => downloadProgress = _progress);

      // Check isNaN because for some reason sometimes fileDetails fail and return
      // broken and _progress operation become 0/0
      // This happen sometimes afet finish the download
      if (downloadProgress >= 1.0 || downloadProgress.isNaN ) {
        print('download finished.');
        downloadProgress = 1.0;
        _checkDownloadFinished(timer);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 5),
      child: isHashing
          ? Container(
              width: MediaQuery.of(context).size.width - 39,
              height: 12,
              child: ClipRRect(
                borderRadius: BorderRadius.all(Radius.circular(8)),
                child: LinearProgressIndicator(
                  valueColor: AlwaysStoppedAnimation<Color>(ACCENT_COLOR),
                  backgroundColor: REMOTE_COLOR,
                ),
              ),
            )
          : LinearPercentIndicator(
              width: MediaQuery.of(context).size.width - 30,
              linearStrokeCap: LinearStrokeCap.roundAll,
              lineHeight: 12.0,
              percent: downloadProgress,
              center: Text(
                  S
                      .of(context)
                      .progresPercent((downloadProgress * 100).round()),
                  style: TextStyle(fontSize: 9.0, color: WHITE_COLOR)),
              backgroundColor: REMOTE_COLOR,
              progressColor: ACCENT_COLOR,
              animateFromLastPercent: true,
              animation: true,
              animationDuration: 1000,
            ),
    );
  }
}
