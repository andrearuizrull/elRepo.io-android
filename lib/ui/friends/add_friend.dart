/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import 'package:barcode_scan/barcode_scan.dart';
import 'package:elRepoIo/routes.dart';
import 'package:elRepoIo/ui/common/avatar_or_icon.dart';
import 'package:elRepoIo/ui/common/ui_utils.dart';
import 'package:elRepoIo/ui/friends/peer_card.dart';
import 'package:elRepoIo/ui/providers/identities_provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:retroshare_dart_wrapper/retroshare.dart' as rs;
import 'package:flutter/services.dart';

import 'package:elRepoIo/constants.dart';
import 'package:elRepoIo/generated/l10n.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:share_plus/share_plus.dart';

class AddFriendsScreen extends ConsumerStatefulWidget {
  final AddFriendArguments arguments;

  const AddFriendsScreen({Key key, this.arguments}) : super(key: key);

  @override
  _AddFriendsScreenState createState() => _AddFriendsScreenState();
}

class _AddFriendsScreenState extends ConsumerState<AddFriendsScreen> {
  /// Own Retrohsare short invite string
  String _rsInvite;

  /// Used to space the column items
  final double _spacing = 15.0;

  /// Used to store friends invite
  final TextEditingController _teFriendInvite = TextEditingController();

  /// Future with the result of parse the _teFriendInvite.text
  Future<Map<String, dynamic>> _parseShortInviteResult;

  /// Used to know if there are an error parsing the invite
  bool _errorParsingInvite = false;

  @override
  void dispose() {
    _teFriendInvite.dispose();
    super.dispose();
  }

  Future<void> _scan() async {
    var codeSanner = await BarcodeScanner.scan();
    print('Scan result');
    print(codeSanner.rawContent);
    _teFriendInvite.text = codeSanner.rawContent;
    setState(() {});
  }

  void _getShortInviteInfo() {
    if (_teFriendInvite.text.isNotEmpty) {
      _parseShortInviteResult =
          rs.RsPeers.parseShortInvite(_teFriendInvite.text);
      _parseShortInviteResult.then((value) {
        _errorParsingInvite = false;
        setState(() {});
      }).onError((error, stackTrace) {
        _errorParsingInvite = true;
        setState(() {});
      });
      setState(() {});
    }
  }

  @override
  void initState() {
    rs.RsPeers.getShortInvite(baseUrl: 'https://retroshare.me/addfriend')
        .then((value) {
      _rsInvite = value;
      setState(() {});
    });
    _teFriendInvite.addListener(_getShortInviteInfo);
    _getShortInviteInfo();
    if (widget.arguments != null) {
      _teFriendInvite.text = widget.arguments.rsInvite;
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final ownId = ref.watch(ownIdentityProvider);
    return Scaffold(
        appBar: AppBar(
          backgroundColor: PURPLE_COLOR.withOpacity(0.9),
          shadowColor: REMOTE_COLOR,
          brightness: Brightness.dark,
          elevation: 0,
          leading: IconButton(
            onPressed: () {
              Navigator.of(context).pop();
            },
            icon: Icon(
              Icons.arrow_back_rounded,
              color: WHITE_COLOR,
            ),
          ),
          title: Text(
            'Exchange friendship', // todo(intl)
            style: TextStyle(fontWeight: FontWeight.w800, color: WHITE_COLOR),
          ),
        ),
        body: SingleChildScrollView(
          padding: const EdgeInsets.symmetric(horizontal: 25, vertical: 16),
          child: Column(
            children: [
              SizedBox(height: _spacing),
              Text(
                S.of(context).addFriendInstructions,
                style: TextStyle(
                  color: REMOTE_COLOR,
                ),
              ),
              SizedBox(height: _spacing),
              Text(
                S.of(context).addFriendYourIdTitle,
                style: TextStyle(
                  fontSize: 20,
                ),
              ),
              SizedBox(height: _spacing),
              Text(
                S.of(context).addFriendYourIdExplanation,
                style: TextStyle(
                  color: REMOTE_COLOR,
                ),
              ),
              SizedBox(height: _spacing),
              GestureDetector(
                onLongPress: () {
                  copyToClipboard(context, _rsInvite, 'Invite copied!'); // todo(intl)
                },
                child: Card(
                  elevation: 8.0,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0)),
                  color: LOCAL_COLOR,
                  child: ListTile(
                    title: Text(
                      rs.authLocationName,
                      style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                    subtitle: Text(
                      'Long press to copy...', // todo(intl)
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 12,
                        fontStyle: FontStyle.italic
                        // fontWeight: FontWeight.w100,
                      ),
                    ),
                    leading: AvatarOrIcon(
                      ownId.avatar,
                      cacheKey: ownId.mId,
                      altText: ownId.name,
                    ),
                    trailing: Wrap(spacing: 15.0, children: [
                      GestureDetector(
                        onTap: () {
                          if (_rsInvite != null) Share.share(_rsInvite);
                        },
                        child: Icon(Icons.share, color: WHITE_COLOR),
                      ),
                      GestureDetector(
                        onTap: () {
                          if (_rsInvite != null) {
                            var alert = AlertDialog(
                              // title: Text("Share your invite"),
                              content: Container(
                                width: 280, height: 280,
                                child: QrImage(
                                  data: _rsInvite,
                                ),
                              ),
                              // actions: IconButton,
                            );
                            showDialog(
                              context: context,
                              builder: (BuildContext context) {
                                return alert;
                              },
                            );
                          }
                        },
                        child: Icon(Icons.qr_code, color: WHITE_COLOR),)
                    ]),
                  ),
                ),
              ),
              SizedBox(height: _spacing),
              Text(
                S.of(context).addFriendPeerIdTitle,
                style: TextStyle(
                  fontSize: 20,
                ),
              ),
              SizedBox(height: _spacing),
              Text(
                S.of(context).addFriendPeerIdExplanation,
                style: TextStyle(
                  color: REMOTE_COLOR,
                ),
              ),
              Text(
                '\nOr scan a QR code from your camera.', // todo(intl)
                style: TextStyle(
                  color: REMOTE_COLOR,
                ),
              ),
              TextField(
                controller: _teFriendInvite,
                decoration: InputDecoration(
                  suffixIcon: RawMaterialButton(
                    onPressed: () async => await _scan(),
                    elevation: 2.0,
                    fillColor: LOCAL_COLOR,
                    padding: EdgeInsets.all(10.0),
                    shape: CircleBorder(),
                    child: Icon( Icons.camera_alt, size: 25.0, color: Colors.white ,),
                  ),
                  icon: Icon(Icons.person_add, color: LOCAL_COLOR),
                  labelText: S.of(context).addFriendHint,
                ),
              ),
              SizedBox(height: _spacing),
              ElevatedButton(
                onPressed: () async {
                  if (!_errorParsingInvite) {
                    rs.RsPeers.acceptShortInvite(_teFriendInvite.text);
                    Navigator.of(context).pop();
                  }
                },
                style: ButtonStyle(
                    backgroundColor: _errorParsingInvite
                        ? MaterialStateProperty.all(NEUTRAL_COLOR)
                        : MaterialStateProperty.all(LOCAL_COLOR)),
                child: Text(S.of(context).addFriendButton),
              ),
              SizedBox(height: _spacing),
              FutureBuilder(
                  future: _parseShortInviteResult,
                  builder: (context, AsyncSnapshot snapshot) {
                    if (snapshot.connectionState != ConnectionState.done &&
                        _teFriendInvite.text.isNotEmpty) {
                      return loadingBox();
                    }
                    if (snapshot.hasError && _teFriendInvite.text.isNotEmpty) {
                      print('Error: ${snapshot.error}');
                      return Center(
                        child: Text(S.of(context).cannotLoadContent),
                      );
                    }
                    if (_teFriendInvite.text.isEmpty ||
                        snapshot.data == null ||
                        snapshot.data.length == 0) {
                      return Text('');
                    }
                    return SizedBox(
                        width:
                        1000, // Trying to avoid Failed assertion 'constraints.hasBoundedWidth': is not true.
                        child: PeerCard(
                          null,
                          inviteData: snapshot.data,
                          buttonsEnabled: false,
                        ));
                  }),
            ],
          ),
        ));
  }
}
