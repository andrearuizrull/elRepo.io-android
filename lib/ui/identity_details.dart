/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import 'package:elRepoIo/routes.dart';
import 'package:elRepoIo/ui/common/avatar_or_icon.dart';
import 'package:elRepoIo/ui/common/buttons.dart';
import 'package:elRepoIo/ui/common/posts_teaser.dart';
import 'package:elRepoIo/ui/common/ui_utils.dart';
import 'package:elRepoIo/ui/common/updatable_future_builder.dart';
import 'package:elRepoIo/ui/common/updatable_list_page.dart';
import 'package:elRepoIo/ui/providers/identities_following.dart';
import 'package:elRepoIo/ui/providers/identities_provider.dart';
import 'package:elRepoIo/ui/providers/teasers_providers.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'dart:async';
import 'package:elrepo_lib/repo.dart' as repo;

import 'package:elRepoIo/constants.dart';
import 'package:elRepoIo/generated/l10n.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:retroshare_dart_wrapper/rs_models.dart';

class IdentityDetailsScreen extends ConsumerStatefulWidget {
  final IdentityArguments args;
  IdentityDetailsScreen(this.args);

  @override
  _IdentityDetailsScreenState createState() => _IdentityDetailsScreenState();
}

class _IdentityDetailsScreenState extends ConsumerState<IdentityDetailsScreen> {
  Identity identityDetails;
  // Result of getForumsSummaries for this user
  List<dynamic> userForums = [];
  bool alreadyFollowing;
  int visibleMsgCount = -1;

  void getUserForumsInfo () {
    repo.getUserForum(widget.args.identityId).then((forums) {
      userForums = forums;
      visibleMsgCount = 0;
      // Subscribe to user forums to see a preview. If you don't follow it,
      // unsubscribe on dispose
      for (var forum in forums) {
        visibleMsgCount += forum['mVisibleMsgCount'] as int;
        // FTR: Could happen that you can see a circled user forum after following it, causing not all user forums
        // will have mSubscribeFlags == 4
        if (forum['mSubscribeFlags'] != 4) {
          repo.subscribeToForum(forum['mGroupId']);
        }
      }
      // Set state to update the visibleMsgCount
      setState(() { });
    });
  }

  void setIdentityDetails(Identity idDetails) {
    identityDetails = idDetails;
    alreadyFollowing = idDetails.isContact;
  }

  @override
  void initState() {
    print('Identity details for ${widget.args.identityId}');
    widget.args.idDetails == null
        ? repo
        .getIdDetails(widget.args.identityId).then(
            (idDetails) => setState(() => setIdentityDetails(idDetails)))
        : setIdentityDetails(widget.args.idDetails);
    getUserForumsInfo();
    super.initState();
  }

  @override
  void dispose() {
    // Unsubscribe user forums if we are not following it
    if(!alreadyFollowing) repo.subscribeToForums(userForums, false);
    super.dispose();
  }

  void followAction(String identityID, bool follow) {
    repo.followUser(identityID, follow);
    alreadyFollowing = follow;
    ref.watch(followingCardProvider.notifier).update();
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      appBar: AppBar(
        centerTitle: false,
        title: Text(
          'elRepo.io',
          style: TextStyle(
            color: WHITE_COLOR,
          ),
        ),
        backgroundColor: PURPLE_COLOR.withOpacity(0.9),
        shadowColor: REMOTE_COLOR,
        brightness: Brightness.dark,
        elevation: 0,
      ),
      body: identityDetailsWidget(),
    );
  }

  Widget identityDetailsWidget() {
    var idProvider = identityPostsProvider(widget.args.identityId);
    void idProviderUpdate () => ref.watch(idProvider.notifier).update();
    return Center(
        child: ListView(children: <Widget>[
          SizedBox(
            height: 20,
          ),
          ListTile(
              leading:
              Container(
                child: AvatarOrIcon(
                  identityDetails == null ? '' : identityDetails.avatar,
                  altText: identityDetails == null ? '' : identityDetails.name ?? '',
                  cacheKey: widget.args.identityId ?? identityDetails.mId ?? '',
                ),
              ),
              // ),
              title: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    identityDetails != null
                        ? identityDetails.name
                        : '...',
                    style: TextStyle(
                      fontSize: 24,
                      color: ACCENT_COLOR,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                ],
              ),
              subtitle: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  TimeText(identityDetails?.mLastUsageTS?.xint64 ?? 1),
                  IconTextWrap(
                    visibleMsgCount >= 0
                        ? '$visibleMsgCount publications' : '..'
                    , iconData: Icons.chat,), // todo(intl)
                  GestureDetector(
                    onLongPress: () => copyToClipboard(context,  identityDetails.mId, 'Copied!'), // todo(intl),
                    child: Text(
                        identityDetails != null ? identityDetails.mId : '',
                        style: TextStyle(
                          fontSize: 14,
                          color: NEUTRAL_COLOR,
                        )
                    ),
                  ),
                  if (alreadyFollowing != null &&
                      !alreadyFollowing &&
                      identityDetails != null)
                    RaisedButton(
                      //color: !alreadyFollowing ? LOCAL_COLOR : REMOTE_COLOR,
                      onPressed: () {
                        followAction(identityDetails.mId, !alreadyFollowing);
                      },
                      textColor: Colors.white,
                      color: LOCAL_COLOR,
                      child: Text(S.of(context).follow),
                    )
                  else if (alreadyFollowing != null &&
                      alreadyFollowing && identityDetails != null)
                    RaisedButton(
                      disabledTextColor: Colors.white,
                      disabledColor: REMOTE_COLOR,
                      onPressed: () {
                        followAction(identityDetails.mId, !alreadyFollowing);
                      },
                      child: Text(S.of(context).following),
                    )
                  else
                    RaisedButton(
                      child: Text('...'),
                      disabledTextColor: Colors.white,
                      disabledColor: NEUTRAL_COLOR,
                    ),
                ],
              )),
          SizedBox(
            height: MediaQuery.of(context).size.height * .01,
          ),
          Container(
              height: MediaQuery.of(context).size.height * .72,
              child: UpdatableListPage<RsMsgMetaData>(
                // todo: use this provider as variable to DRY
                idProvider,
                callbackWidget: (data) =>  PostTeaserCard(data,),
                timerCallback: (ref, t) => idProviderUpdate(),
                noDataWidget: (ref) => SynchronizingPostsButton(
                  S.of(context).commonButtonsSynchronizePosts,
                  onPressed: idProviderUpdate,
                )
              )
            )
        ]));
  }
}
