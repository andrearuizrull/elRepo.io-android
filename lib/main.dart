/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import 'package:elRepoIo/app_builder.dart';
import 'package:elRepoIo/platform_specific/android.dart';
import 'package:elRepoIo/routes.dart';
import 'package:elRepoIo/ui/providers/theme_state.dart';
import 'package:elRepoIo/user_preferences.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'dart:async';
import 'package:sentry/sentry.dart';
import 'package:connectivity/connectivity.dart';

import 'package:elRepoIo/RsServiceControl/rscontrol.dart' as rscontrol;
import 'package:flutter/foundation.dart' show defaultTargetPlatform, TargetPlatform;

import 'package:retroshare_dart_wrapper/src/exceptions.dart' show LoginException, AuthFailedException;

import 'generated/l10n.dart';

void main() async {
  // Used to don't send to sentry lots of times the same exception when is in
  // the autostart loop
  var loginExceptions = 0, authExceptions = 0, maxExceptionsRetries = 1;
  runZonedGuarded<Future<void>>(
        () async {
      await Sentry.init(
            (options) {
          options.dsn =
          'https://f3dadd9fc2324d8fbf3655bfafc3895c@o448455.ingest.sentry.io/5429863';
          options.beforeSend = (SentryEvent event, {dynamic hint}){
            if(USER_PREFERENCES.SENTRY_SEND_REPORTS ?? false) {
              return event;
            }
            return null;
          };
        },
      );
      // TODO(nicoechaniz): propperly dispose of the subscription on app close
      WidgetsFlutterBinding.ensureInitialized();
      final _connectivity = Connectivity();
      var connectivitySubscription =  _connectivity.onConnectivityChanged
          .listen(rscontrol.updateConnectionStatus);
      runApp(const ProviderScope(child: ElRepoApp()));
    }, (Object error, StackTrace stackTrace) {
      print(error);
      print(stackTrace);
      if(error is LoginException) {
        loginExceptions++;
      } else if(error is AuthFailedException) {
        authExceptions++;
      } else {
        // Restart the counter if any other exception
        loginExceptions = 0;
        authExceptions = 0;
      }
      if(loginExceptions>=maxExceptionsRetries || authExceptions>=maxExceptionsRetries) return;
      Sentry.captureException(error, stackTrace: stackTrace);
    },
  );
}

class ElRepoApp extends ConsumerStatefulWidget {
  const ElRepoApp();

  @override
  _ElRepoAppState createState() => _ElRepoAppState();
}

/// Used when receive a sharing intent to navigate to publish screen without
/// using context
///
/// Used by android sharing intents.
///
/// See https://github.com/KasemJaffer/receive_sharing_intent/issues/62#issuecomment-691011192
final GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();

class _ElRepoAppState extends ConsumerState<ElRepoApp> {

  HandleSharingIntent sharingIntent;

  @override
  void initState() {
    super.initState();
    if(defaultTargetPlatform == TargetPlatform.android) {
      sharingIntent = HandleSharingIntent(navigatorKey);
      sharingIntent.startReceiveSharingIntent();
    }
  }

  @override
  Widget build(BuildContext context) {
    final themeMode = ref.watch(themeNotifierProvider.select((value) => value.currentTheme));
    return AppBuilder(
      builder: (context) {
        return MaterialApp(
          localizationsDelegates: [
            S.delegate,
            GlobalMaterialLocalizations.delegate,
            GlobalWidgetsLocalizations.delegate,
          ],
          supportedLocales: S.delegate.supportedLocales,
          theme: themeMode,
          initialRoute: '/',
          onGenerateRoute: RouteGenerator.generateRoute,
          navigatorKey: navigatorKey,
        );
      },
    );
  }
}

