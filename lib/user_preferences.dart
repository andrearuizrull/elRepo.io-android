/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import 'dart:convert';
import 'dart:ui';

import 'package:elRepoIo/constants.dart';
import 'package:elRepoIo/generated/l10n.dart';
import 'package:localstorage/localstorage.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';


class USER_PREFERENCES  {
  /// This variable is used to deactivate on runtime sentry reports
  static bool SENTRY_SEND_REPORTS = false;
  static String PREFERRED_LANGUAGE = '';
  static bool ACTIVATE_PERCEPTUAL_SEARCH = false;
  static RsCredentials RS_CREDENTIALS;
}

var storage = LocalStorage(STORAGE_NAME);
// TODO: Migrate all to secure_storage plugin when web support will be released
var secureStorage = FlutterSecureStorage();

Future<LocalStorage> getReadyStorage() async {
  await storage.ready;
  return storage;
}

/// Used to load user preferences from local storage to on memory
Future<void> initializePreferences () async{
  await getReadyStorage();
  // storage.setItem(SENTRY_SEND_REPORTS, null ); // Uncomment this to reset sentry status
  USER_PREFERENCES.SENTRY_SEND_REPORTS = storage.getItem(SENTRY_SEND_REPORTS);
  USER_PREFERENCES.PREFERRED_LANGUAGE = storage.getItem(PREFERRED_LANGUAGE);
  if(USER_PREFERENCES.PREFERRED_LANGUAGE?.isNotEmpty ?? false) {
    setPreferredLanguage(Locale(USER_PREFERENCES.PREFERRED_LANGUAGE));
  }
  USER_PREFERENCES.ACTIVATE_PERCEPTUAL_SEARCH = storage.getItem(ACTIVATE_PERCEPTUAL_SEARCH) ?? false;
  var creds = await getRsCredentials();
  if(creds != null) {
    USER_PREFERENCES.RS_CREDENTIALS = creds;
  }
}

void setPreferredLanguage(Locale locale){
  S.load(locale);
  USER_PREFERENCES.PREFERRED_LANGUAGE = locale.languageCode;
  storage.setItem(PREFERRED_LANGUAGE, USER_PREFERENCES.PREFERRED_LANGUAGE );
}

/// Modify global variable sentry configuration and store it to local storage
void setSentrySendReports(bool value) {
  USER_PREFERENCES.SENTRY_SEND_REPORTS = value;
  storage.setItem(SENTRY_SEND_REPORTS, USER_PREFERENCES.SENTRY_SEND_REPORTS );
}

/// Activate or deactivate "search similar" button on post details for images
void setPerceptualSearch(bool value) {
  USER_PREFERENCES.ACTIVATE_PERCEPTUAL_SEARCH = value;
  storage.setItem(ACTIVATE_PERCEPTUAL_SEARCH, USER_PREFERENCES.ACTIVATE_PERCEPTUAL_SEARCH );
}

/// Use secure storage to store RS credentials to login automatically
void setRsCredentials(Map<String, dynamic> location, String secret) {
  print('Set user preferences for $location');
  USER_PREFERENCES.RS_CREDENTIALS = RsCredentials(location, secret);
  secureStorage.write(key: RS_CREDENTIALS, value: json.encode(USER_PREFERENCES.RS_CREDENTIALS));
}
void deleteRsCredentials( ) {
  print('Deleting user credentials');
  USER_PREFERENCES.RS_CREDENTIALS = null;
  secureStorage.write(key: RS_CREDENTIALS, value: null);
}

Future<RsCredentials> getRsCredentials() async {
  var rawCreds = await secureStorage.read(key: RS_CREDENTIALS);
  if(rawCreds != null) {
    var creds = RsCredentials.fromJson(json.decode(rawCreds));
    if (creds.firstUsageTs != null &&
         DateTime.now().millisecondsSinceEpoch - creds.firstUsageTs <  RsCredentials.expiresIn) {
      return creds;
    } else {
      deleteRsCredentials();
    }
  }
  return null;
}

/// Util class to manage credentials basics for auto login
class RsCredentials{
  // Map got when getDefaultLocation
  Map<String, dynamic> location;
  String secret;
  // Store when this credentials are stored for first time to give a time alive
  int firstUsageTs;
  static int expiresIn = 86400000;  // one day to Milliseconds
  RsCredentials(this.location, this.secret){
    firstUsageTs = DateTime.now().millisecondsSinceEpoch;
  }
  Map<String, dynamic> toJson() => {
    'location' : location,
    'secret' : secret,
    'firstUsageTs' : firstUsageTs};
  RsCredentials.fromJson(Map<String, dynamic> jsonObj) {
    location = jsonObj['location'];
    secret = jsonObj['secret'];
    firstUsageTs = jsonObj['firstUsageTs'];
  }
}