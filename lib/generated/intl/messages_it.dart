// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a it locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'it';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static _notInlinedMessages(_) => <String, Function> {
    "appIntro" : MessageLookupByLibrary.simpleMessage("Introduzione all\'app"),
    "appSettings" : MessageLookupByLibrary.simpleMessage("Impostazioni app"),
    "attemptconnection" : MessageLookupByLibrary.simpleMessage("Tenta la connessione"),
    "automaticpeering" : MessageLookupByLibrary.simpleMessage("Peering automatico"),
    "backup" : MessageLookupByLibrary.simpleMessage("Crea un backup"),
    "backupText" : MessageLookupByLibrary.simpleMessage("I dati del tuo account verranno copiati in un file zip e ti verrà presentato un menu per condividerlo.\n\nScegli una destinazione che consideri sicura e affidabile.\n\nUna volta condiviso il backup lo farai è necessario accedere di nuovo a elRepo.io\n\nSe sarà necessario ripristinare questo backup, sarà necessario scaricare il file zip\n\ne fornirlo nella schermata di registrazione."),
    "browse" : MessageLookupByLibrary.simpleMessage("Sfoglia"),
    "cancel" : MessageLookupByLibrary.simpleMessage("Annulla"),
    "cannotLoadContent" : MessageLookupByLibrary.simpleMessage("Impossibile caricare il contenuto."),
    "checkItOut" : MessageLookupByLibrary.simpleMessage("Dai un\'occhiata"),
    "chooseFile" : MessageLookupByLibrary.simpleMessage("Scegli file"),
    "circle" : MessageLookupByLibrary.simpleMessage("Cerchio"),
    "circleErrorNameEmpty" : MessageLookupByLibrary.simpleMessage("Il nome della cerchia non può essere vuoto."),
    "circleErrorNoContactsYet" : MessageLookupByLibrary.simpleMessage("Nessun contatto ancora, attendi la propagazione."),
    "circleErrorNoIds" : MessageLookupByLibrary.simpleMessage("Nessun ID selezionato."),
    "circleErrorSelectPerson" : MessageLookupByLibrary.simpleMessage("Devi selezionare almeno una persona da aggiungere alla cerchia."),
    "circleInvitePeople" : MessageLookupByLibrary.simpleMessage("E invitare questa gente?"),
    "circleSelectPeople" : MessageLookupByLibrary.simpleMessage("Seleziona le persone da invitare alla tua cerchia."),
    "circleWithMemberList" : MessageLookupByLibrary.simpleMessage("Con questa lista dei membri?"),
    "circlesTitle" : MessageLookupByLibrary.simpleMessage("Cerchi"),
    "comment" : MessageLookupByLibrary.simpleMessage("Commento"),
    "commentPosted" : MessageLookupByLibrary.simpleMessage("Commento pubblicato"),
    "connect" : MessageLookupByLibrary.simpleMessage("Collegare"),
    "cont" : MessageLookupByLibrary.simpleMessage("Continua"),
    "contentPublished" : MessageLookupByLibrary.simpleMessage("Contenuto pubblicato"),
    "create" : MessageLookupByLibrary.simpleMessage("Creare"),
    "createAccount" : MessageLookupByLibrary.simpleMessage("Crea un account"),
    "createandshare" : MessageLookupByLibrary.simpleMessage("Crea e condividi file di backup."),
    "creatingAccount" : MessageLookupByLibrary.simpleMessage("... creazione dell\'account. \n Questa operazione potrebbe richiedere del tempo."),
    "darkMode" : MessageLookupByLibrary.simpleMessage("Tema scuro"),
    "delete" : MessageLookupByLibrary.simpleMessage("Elimina"),
    "description" : MessageLookupByLibrary.simpleMessage("Descrizione"),
    "details" : MessageLookupByLibrary.simpleMessage("Dettagli"),
    "disconnect" : MessageLookupByLibrary.simpleMessage("Disconnetti"),
    "discover" : MessageLookupByLibrary.simpleMessage("Scoprire"),
    "edit" : MessageLookupByLibrary.simpleMessage("Modifica"),
    "elRepoIoIsLoading" : MessageLookupByLibrary.simpleMessage("eelRepo.io sta caricando..."),
    "error" : MessageLookupByLibrary.simpleMessage("Error"),
    "follow" : MessageLookupByLibrary.simpleMessage("Segui"),
    "following" : MessageLookupByLibrary.simpleMessage("Seguito"),
    "knownnodes" : MessageLookupByLibrary.simpleMessage("Nodi noti"),
    "language" : MessageLookupByLibrary.simpleMessage("Scegli una lingua"),
    "leave" : MessageLookupByLibrary.simpleMessage("Partire"),
    "localRepo" : MessageLookupByLibrary.simpleMessage("local\nrepo"),
    "loggingIn" : MessageLookupByLibrary.simpleMessage("Accesso. Questa operazione può richiedere del tempo ..."),
    "loggingInAs" : MessageLookupByLibrary.simpleMessage("Accesso come"),
    "login" : MessageLookupByLibrary.simpleMessage("Entra"),
    "loginFailed" : MessageLookupByLibrary.simpleMessage("Accesso non riuscito. Riprova."),
    "name" : MessageLookupByLibrary.simpleMessage("Nome"),
    "network" : MessageLookupByLibrary.simpleMessage("La tua rete elRepo.io"),
    "participants" : MessageLookupByLibrary.simpleMessage("Partecipanti"),
    "password" : MessageLookupByLibrary.simpleMessage("Password"),
    "passwordCannotBeEmpty" : MessageLookupByLibrary.simpleMessage("La password non può essere vuota."),
    "peeringdescription" : MessageLookupByLibrary.simpleMessage("Se attivato ti connetti automaticamente con altri nodi elRepo.io nella tua rete locale."),
    "postingComment" : MessageLookupByLibrary.simpleMessage("Pubblicazione del comment"),
    "publish" : MessageLookupByLibrary.simpleMessage("Pubblica"),
    "publishingContent" : MessageLookupByLibrary.simpleMessage("Pubblicazione di contenuti...\nSe hai aggiunto file pesanti, il completamento potrebbe richiedere diversi minuti."),
    "remoteControl" : MessageLookupByLibrary.simpleMessage("Remote Control"),
    "remoteControlerror" : MessageLookupByLibrary.simpleMessage("ERRORE: impossibile connettersi al nodo remoto!"),
    "remoteCtrl" : MessageLookupByLibrary.simpleMessage("Controllo remoto"),
    "remoteRules" : MessageLookupByLibrary.simpleMessage("Devi specificare l\'URL completo dell\'API di controllo del nodo remoto."),
    "savingContentToLocalRepo" : MessageLookupByLibrary.simpleMessage("Download contenuto in corso..."),
    "settings" : MessageLookupByLibrary.simpleMessage("Impostazioni"),
    "signOut" : MessageLookupByLibrary.simpleMessage("ESCI"),
    "signUp" : MessageLookupByLibrary.simpleMessage("Registrati"),
    "tags" : MessageLookupByLibrary.simpleMessage("tags"),
    "title" : MessageLookupByLibrary.simpleMessage("Titolo"),
    "titleCannotBeEmpty" : MessageLookupByLibrary.simpleMessage("Il titolo non può essere vuoto."),
    "username" : MessageLookupByLibrary.simpleMessage("Nome utente"),
    "usernameCannotBeEmpty" : MessageLookupByLibrary.simpleMessage("Il nome utente non può essere vuoto."),
    "youCanAddHashTags" : MessageLookupByLibrary.simpleMessage("Suggerimento: puoi aggiungere #HashTags nella descrizione per scoprire più facilmente il tuo post."),
    "your" : MessageLookupByLibrary.simpleMessage("Tuo")
  };
}
