/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import 'package:elRepoIo/routes.dart';
import 'package:elRepoIo/ui/circles/show_circles.dart';
import 'package:elRepoIo/ui/common/identity_card.dart';
import 'package:elRepoIo/ui/common/loading_dialog.dart';
import 'package:elRepoIo/ui/common/ui_utils.dart';
import 'package:elRepoIo/ui/providers/circles_provider.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:retroshare_dart_wrapper/rs_models.dart'
    show Identity, RsGxsCircleSubscriptionFlags;
import 'package:flutter/material.dart';
import 'package:retroshare_dart_wrapper/retroshare.dart' as rs;
import 'package:elrepo_lib/repo.dart' as repo;
import 'package:elRepoIo/generated/l10n.dart';
import 'package:elRepoIo/constants.dart';
import '../common/search_bar.dart';

class CircleDetailsScreen extends ConsumerStatefulWidget {
  CircleArguments args;
  CircleDetailsScreen(this.args);

  @override
  _CircleDetailsScreenState createState() => _CircleDetailsScreenState();
}

class _CircleDetailsScreenState extends ConsumerState<CircleDetailsScreen>
    with TickerProviderStateMixin {
  String _appBarTitle, _appBarSubtitle;

  // Text editing controller for the search bar
  final TextEditingController _teController = TextEditingController();

  // Text editing controller for the circle name
  final TextEditingController _teCircleName = TextEditingController();

  // Control searching state
  bool _isSearching = false;

  // List of identities
  List<Identity> identities = [];
  List<Identity> selected = [];

  // Show details only mode
  bool _detailsOnly = false;

  List<Widget> _createIdentityTeaser() {
    return _detailsOnly
        ? _createTeaserListFromIdentitesInfo()
        : _createTeaserListFromSearch();
  }

  // Function that creates widgets with the results of search
  List<Widget> _createTeaserListFromSearch() {

    var search = List.from(identities.where((element) =>
        element.name.toLowerCase().contains(_teController.text.toLowerCase()) ||
            element.mId.toLowerCase().contains(_teController.text.toLowerCase())
    ));
    // var search = List.from(identities.where((element) => element.name
    //     .toLowerCase()
    //     .contains(_teController.text.toLowerCase())));
    return List<Widget>.generate(search.length, (int index) {
      return checkboxIdentityCard(search[index]);
    });
  }

  // Create teasers list for identites info
  List<Widget> _createTeaserListFromIdentitesInfo() {
    return List<Widget>.generate(identities.length, (int index) {
      return IdentityCard(identities[index]);
    });
  }

  /// Show list of chips hiding tier one identities if present
  List<Widget> _selectedChipsList([bool inputChip = true]) {
    return selected.isNotEmpty
        ? [
            for ( var selectedIdentity in selected )
              // Added tier1 nodes shouldn't be shown
              if(!repo.isIdOwnedByTier(selectedIdentity) )
                selectedChip(selectedIdentity, inputChip)
          ]
        : [Text(S.of(context).circleErrorNoIds)];
  }

  @override
  void initState() {
    // Execute below callback once set state can be done
    WidgetsBinding.instance.addPostFrameCallback((_) {
      _appBarTitle ??= S.of(context).create + ' ' + S.of(context).circle;
      _appBarSubtitle ??= S.of(context).circleSelectPeople;
    });

    if (widget.args != null) {
      _detailsOnly = widget.args.detailsOnly;
      _teCircleName.text = widget.args.circleDetail['mCircleName'];
      _appBarSubtitle = widget.args.subtitle;
      _appBarTitle = widget.args.title;

      _isSearching = true;

      if (_detailsOnly) {
        // Get identity details
        var ids = List<String>.from(widget
            .args.circleDetail['mSubscriptionFlags']
            .where((flag) =>
                flag['value'] == 7 ||
                flag['value'] & RsGxsCircleSubscriptionFlags.IN_ADMIN_LIST ==
                    RsGxsCircleSubscriptionFlags.IN_ADMIN_LIST)
            .map((flag) => flag['key'])
            .toList());
        rs.RsIdentity.getIdentitiesInfo(ids).then((res) async {
          res.removeWhere((id) {
            if(repo.isIdOwnedByTier(id)){
              return true;
            }
            // Add flags if we are not deleting it
            var flag = widget.args.circleDetail['mSubscriptionFlags']
                // I think here we are deleting pending invited members, but not sure
                .firstWhere((flag) => flag['key'] == id.mId,
                    orElse: () => null);
            id.rawIdentityInfo['flag'] = flag; // NotUsed?
            return false;
          });
          setState(() {
            _isSearching = false;
            identities = res;
          });
        });
      }
    }
    if (!_detailsOnly) {
      _teController.addListener(searchAction);
      repo.getIdentitiesSummaries().then((ids) {
        for (var id in ids) {
          id.rawIdentitySummary['isCheck'] = false;
          if (widget.args != null) {
            for (var flag in widget.args.circleDetail['mSubscriptionFlags']) {
              if (flag['key'] == id.mId &&
                  (flag['value'] == 7 ||
                      flag['value'] &
                              RsGxsCircleSubscriptionFlags.IN_ADMIN_LIST ==
                          RsGxsCircleSubscriptionFlags.IN_ADMIN_LIST)) {
                id.rawIdentitySummary['isCheck'] = true;
                selected.add(id);
              }
            }
          }
        }
        setState(() {
          _isSearching = false;
          identities = ids;
        });
      });
    }
    super.initState();
  }

  void searchAction() async {
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: Visibility(
        visible: !_detailsOnly,
        child: FloatingActionButton(
          onPressed: () {
            widget.args != null
                ? showCircleCreateEditDialog(context, _teCircleName.text,
                    widget.args.circleDetail['mCircleId'])
                : showCircleCreateEditDialog(
                    context,
                    _teCircleName.text,
                  );
          },
          backgroundColor: REMOTE_COLOR,
          child: Icon(Icons.done),
        ),
      ),
      appBar: AppBar(
        backgroundColor: PURPLE_COLOR.withOpacity(0.9),
        shadowColor: REMOTE_COLOR,
        brightness: Brightness.dark,
        elevation: 0,
        leading: IconButton(
          onPressed: () {
            Navigator.of(context).pop();
          },
          icon: Icon(
            Icons.arrow_back_rounded,
            color: WHITE_COLOR,
          ),
        ),
        title: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              _appBarTitle ?? ' ',
              style: TextStyle(fontWeight: FontWeight.w800, color: WHITE_COLOR),
            ),
            Visibility(
              visible: _appBarSubtitle != null && _appBarSubtitle.isNotEmpty,
              child: Text(
                _appBarSubtitle ?? '',
                style: TextStyle(
                    fontSize: 12.0,
                    fontWeight: FontWeight.normal,
                    color: WHITE_COLOR),
              ),
            ),
          ],
        ),
      ),
      body: Column(
        children: [
          TextFormField(
            enabled: !_detailsOnly,
            controller: _teCircleName,
            decoration: InputDecoration(
              prefixIcon: Icon(Icons.edit, color: LOCAL_COLOR),
              labelText: S.of(context).circle + ' ' + S.of(context).name,
            ),
          ),
          SizedBox(
            height: 16,
            width: double.infinity,
            child: const DecoratedBox(
              decoration: BoxDecoration(color: Colors.white24),
            ),
          ),
          Visibility(
            visible: !_detailsOnly,
            child: Align(
              alignment: Alignment.topLeft,
              child:
                  Wrap(children: [for (var chip in _selectedChipsList()) chip]),
            ),
          ),
          Visibility(
            visible: !_detailsOnly,
            child: Container(
                child: SearchBar(
                  teController: _teController,
                  iconButton: IconButton(
                    onPressed: () => _teController.clear(),
                    icon: Icon(Icons.cancel, color: LOCAL_COLOR),
                  ),
                )),
          ),
          SizedBox(
            height: 16,
            width: double.infinity,
            child: const DecoratedBox(
              decoration: BoxDecoration(color: Colors.white24),
            ),
          ),
          Visibility(visible: _isSearching, child: loadingBox()),
          Visibility(
              visible: !_isSearching && identities.isEmpty,
              child: Text(S.of(context).circleErrorNoContactsYet)),
          Visibility(
            visible: !_isSearching,
            child: Expanded(
              child: ListView(children: [
                for (var resultTeaser in _createIdentityTeaser()) resultTeaser
              ]),
            ),
          ),
        ],
      ),
    );
  }

  Widget selectedChip(Identity identity, [bool inputChip = true]) =>
      inputChip
          ? InputChip(
              padding: EdgeInsets.all(2.0),
              label: Text(
                identity.name,
                style: TextStyle(color: Colors.white),
              ),
              selectedColor: Colors.blue.shade600,
              backgroundColor: LOCAL_COLOR,
              onDeleted: () {
                setState(() {
                  identity.rawIdentitySummary['isCheck'] = false;
                  selected.remove(identity);
                });
              },
            )
          : Chip(
              padding: EdgeInsets.all(2.0),
              label: Text(
                identity.name,
                style: TextStyle(color: Colors.white),
              ),
              backgroundColor: LOCAL_COLOR,
            );

  Widget checkboxIdentityCard(Identity identity) {
    return Card(
      child: Container(
        padding: EdgeInsets.all(5.0),
        child: Column(
          children: <Widget>[
            CheckboxListTile(
                activeColor: LOCAL_COLOR,
                dense: true,
                //font change
                title: Text(
                  identity.name,
                  style: TextStyle(
                      fontSize: 14,
                      fontWeight: FontWeight.w600,
                      letterSpacing: 0.5),
                ),
                subtitle: Text(
                  identity.mId,
                ),
                value: identity.rawIdentitySummary['isCheck'],
                onChanged: (bool val) {
                  setState(() {
                    identity.rawIdentitySummary['isCheck'] = val;
                    val ? selected.add(identity) : selected.remove(identity);
                  });
                })
          ],
        ),
      ),
    );
  }

  /// Show an alert dialog with circle creations/editions
  ///
  /// If [circleId] is not empty, the message and actions performed are not a
  /// circle creation, instead it shows circle edition message and actions.
  void showCircleCreateEditDialog(BuildContext context, String circleName,
      [String circleId = '']) {
    var isEdition = circleId.isNotEmpty;
    Widget title;
    Widget content;
    var actions = <Widget>[
      // Cancel button
      FlatButton(
        onPressed: () {
          Navigator.of(context).pop();
        },
        child: Text(S.of(context).cancel),
      )
    ];

    // Handle errors
    var error = '';
    if (circleName.isEmpty) error += S.of(context).circleErrorNameEmpty + '\n';
    // if (selected.isEmpty) error += S.of(context).circleErrorSelectPerson;

    if (error.isNotEmpty) {
      title = Text(S.of(context).error);
      content = Text(error);
    } else {
      title = isEdition
          ? Text(S.of(context).edit +
              ' ' +
              S.of(context).circle +
              ' "$circleName" ?')
          : Text(S.of(context).create +
              ' ' +
              S.of(context).circle +
              ' "$circleName" ?');
      content = Column(children: [
        isEdition
            ? Text(S.of(context).edit +
                ' ' +
                S.of(context).circle +
                ' "$circleName" ' +
                S.of(context).circleWithMemberList)
            : Text(S.of(context).create +
                ' ' +
                S.of(context).circle +
                ' "$circleName" ' +
                S.of(context).circleInvitePeople),
        Wrap(
          children: _selectedChipsList(),),
      ]);
      // Accept circle creation button
      actions.add(FlatButton(
        onPressed: () async {
          Navigator.pop(context); // Close the dialog
          var pr = ProgressDialog(context, // Show progress dialog
              initialMessage: S.of(context).circleDetailsCreateCircle)
            ..show();
          if (isEdition) {
            await repo.editCircle(circleId, circleName,
                List<String>.from(selected.map((e) => e.mId).toList()));
          } else {
            await repo.createCircle(circleName,
                identity: rs.authIdentityId,
                invitedIds: List<String>.from(selected.map((e) => e.mId).toList())
            );
          }
          pr.update(message: S.of(context).circleDetailsUpdateCircle);
          // todo(kon): prettify this below
          // This is an ugly way to update properly show circles screen
          await repo.getCirclesDetails(); // First, you need to update circle details backend
          await Future.delayed(Duration(seconds: 2)); // Needed to properly refresh [CircleScreen]
          ref.read(circlesProvider.notifier).update();
          pr.update(message: S.of(context).circleDetailsFinishing);
          pr.hide();
          Navigator.pop(context); // And go to  back to CircleScreen
        },
        child: Text(S.of(context).cont),
      ));
    }

    // set up the AlertDialog
    var alert = AlertDialog(
      title: title,
      content: content,
      actions: actions,
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
}
