/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */

package net.altermundi.elrepoio

import io.flutter.embedding.android.FlutterActivity
import io.flutter.embedding.engine.FlutterEngine
import io.flutter.plugins.GeneratedPluginRegistrant


import android.annotation.TargetApi
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Build
import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat

import io.flutter.plugin.common.BinaryMessenger
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugin.common.MethodChannel.MethodCallHandler
import io.flutter.plugin.common.MethodChannel.Result
import io.flutter.plugin.common.PluginRegistry
import io.flutter.plugin.common.PluginRegistry.Registrar

class MainActivity :  FlutterActivity() {
    // Add for foreground project
    private var methodChannel : MethodChannel? = null
    private var permissionHandler: PermissionHandler? = null

    companion object {
        @JvmStatic
        var notificationTitle: String? = "elRepo.io" // Some times flutter doesn't pass this values properly
        @JvmStatic
        var notificationText: String? = "This notification keeps elRepo.io alive"
        @JvmStatic
        var notificationImportance: Int? = NotificationCompat.PRIORITY_DEFAULT
        @JvmStatic
        var notificationIconName: String = "ic_notification_outerborder"
        @JvmStatic
        var notificationIconDefType: String = "drawable"
    }

    override fun configureFlutterEngine(@NonNull flutterEngine: FlutterEngine) {
        super.configureFlutterEngine(flutterEngine)
        GeneratedPluginRegistrant.registerWith(flutterEngine)
        permissionHandler = PermissionHandler(
                activity.applicationContext
        )

        MethodChannel(flutterEngine.dartExecutor.binaryMessenger, RetroShareServiceAndroid.CHANNEL_ID).setMethodCallHandler { call, result ->
//            System.out.println(call.method)
            when (call.method) {
                "getPlatformVersion" -> {
                    result.success("Android ${android.os.Build.VERSION.RELEASE}")
                }
                "hasPermissions" -> {
                    var hasPermissions = permissionHandler!!.isIgnoringBatteryOptimizations()
                            && permissionHandler!!.isWakeLockPermissionGranted()
                    result.success(hasPermissions)
                }
                "initialize" -> {
                    // todo: not working
                    val title = call.argument<String>("android.notificationTitle")
                    val text = call.argument<String>("android.notificationText")
                    val importance = call.argument<Int>("android.notificationImportance")
                    val iconName = call.argument<String>("android.notificationIconName")
                    val iconDefType = call.argument<String>("android.notificationIconDefType")

                    // todo: not working
                    // Set static values so the RetroShareServiceAndroid can use them later on to configure the notification
                    notificationImportance = importance ?: notificationImportance
                    notificationTitle = title ?: notificationTitle
                    notificationText = text ?: text
                    notificationIconName = iconName ?: notificationIconName
                    notificationIconDefType = iconDefType ?: notificationIconDefType

                    // Ensure wake lock permissions are granted
                    if (!permissionHandler!!.isWakeLockPermissionGranted()) {
                        result.error("PermissionError", "Please add the WAKE_LOCK permission to the AndroidManifest.xml in order to use background_sockets.", "")
                    }
//                    // Ensure ignoring battery optimizations is enabled
                    if (!permissionHandler!!.isIgnoringBatteryOptimizations()) {
                        if (activity != null) {
                            permissionHandler!!.requestBatteryOptimizationsOff(result, activity!!)
                        } else {
                            result.error("NoActivityError", "The plugin is not attached to an activity", "The plugin is not attached to an activity. This is required in order to request battery optimization to be off.")
                        }
                    }
                    result.success(true)
                }
                "enableBackgroundExecution" -> {
                    // Ensure all the necessary permissions are granted
                    if (!permissionHandler!!.isWakeLockPermissionGranted()) {
                        result.error("PermissionError", "Please add the WAKE_LOCK permission to the AndroidManifest.xml in order to use background_sockets.", "")
                    } else if (!permissionHandler!!.isIgnoringBatteryOptimizations()) {
                        result.error("PermissionError", "The battery optimizations are not turned off.", "")
                    } else {
                        RetroShareServiceAndroid.start( context )
                        result.success(true)
                    }
                }
                "disableBackgroundExecution" -> {
                    RetroShareServiceAndroid.stop( context )
                    result.success(true)
                }
                else -> {
                    result.notImplemented()
                }
            }
        }
    }
}
