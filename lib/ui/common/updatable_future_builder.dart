/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import 'package:elRepoIo/ui/common/ui_utils.dart' as ui_utils;
import 'package:elRepoIo/ui/common/ui_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:elRepoIo/generated/l10n.dart';

import '../../constants.dart';

class UpdatableFutureBuilderController {
  /// Used to call run future from outside this widget
  void Function([Function dynamicGenerator]) runFuture;
  Future<List<dynamic>> Function() generator;
  /// True when the callbackWidget data is not null;
  bool hasData = false;

}

class UpdatableFutureBuilder extends StatefulWidget {
  final Function generator;

  /// This function will return a Widget that show the AsyncSnapshot data
  final Function callbackWidget;
  final UpdatableFutureBuilderController controller;
  final Widget refreshButton;
  final Widget loadingBox;
  final bool enableLoadingBox;

  const UpdatableFutureBuilder(
      {Key key, this.generator, @required this.callbackWidget, this.controller,
        this.refreshButton, this.loadingBox, this.enableLoadingBox})
      : super(key: key);

  @override
  _UpdatableFutureBuilder createState() => _UpdatableFutureBuilder(controller);
}

class _UpdatableFutureBuilder extends State<UpdatableFutureBuilder> {
  UpdatableFutureBuilderController _controller;
  Future<List<dynamic>> _future;
  Function _generator;

  _UpdatableFutureBuilder(UpdatableFutureBuilderController controller) {
    if (controller != null) {
      controller.runFuture = _runFuture;
      _controller = controller;
    }
  }

  Future<List> _voidGenerator() async => [];

  void _runFuture([dynamicGenerator]) {
    print('Updating future');
    _future =
    // Thought a controller you can pass a function dynamically as generator
    dynamicGenerator != null
        ? dynamicGenerator()
        // Or else, use the generator defined on initState()
        : _generator != null
          ? _generator()
          // If generator is not defined, generate a void list
          : _voidGenerator();
    setState(() {});
  }

  @override
  void initState() {
    // Yo can define the generator using widget o a controller
    _generator = _controller?.generator ?? widget.generator;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    if (_future == null) {
      _runFuture();
    }

    return _ReRunnableFutureBuilder(
        _future,
        onRerun: _runFuture,
        refreshButton: widget.refreshButton,
        loadingBox: widget.loadingBox,
        enableLoadingBox: widget.enableLoadingBox,
        callbackWidget: (context, data) {
          _controller?.hasData = data != null;
          return widget.callbackWidget(context, data, _runFuture);
        });
  }
}

class _ReRunnableFutureBuilder extends StatelessWidget {
  final Future<List<dynamic>> _future;
  final Function onRerun; // Function that run setState
  final Function callbackWidget; // builder caller where pass snapshot data  when no errors
  final _refreshKey = GlobalKey<RefreshIndicatorState>();
  /// Refresh button widget. If have to be declared outside this widget, use the
  /// controller to perform the update
  final Widget refreshButton;
  final Widget loadingBox;
  final bool enableLoadingBox;

  _ReRunnableFutureBuilder(this._future,
      {this.onRerun, @required this.callbackWidget, this.refreshButton,
        this.loadingBox, this.enableLoadingBox});

  Future<void> pullDownRefresh(context) => onRerun();

  Widget pullDownToRefreshButton (context) =>
      NoDataYetButton(callback: () => pullDownRefresh(context),);

  @override
  Widget build(BuildContext context) {
    return RefreshIndicator(
      key: _refreshKey,
      onRefresh: () async => pullDownRefresh(context),
      child: FutureBuilder(
          future: _future,
          builder: (context, AsyncSnapshot snapshot) {
            if (enableLoadingBox ?? true && (snapshot.connectionState != ConnectionState.done)) {
              return loadingBox ?? ui_utils.loadingBox();
            }
            if (snapshot.hasError) {
              print('Error: ${snapshot.error}');
              return Center(
                child: Column(
                  children: [
                    Text(S.of(context).cannotLoadContent),
                    refreshButton ?? pullDownToRefreshButton(context),
                  ],
                ),
              );
            }
            if (snapshot.data == null || snapshot.data.length == 0) {
              return Center(
                  child: refreshButton ?? pullDownToRefreshButton(context),
              );
            }
            return SizedBox(
              width: 1000, // Trying to avoid Failed assertion 'constraints.hasBoundedWidth': is not true.
              child: ListView.builder(
                  scrollDirection: Axis.vertical,
                  shrinkWrap: true,
                  itemCount: snapshot.data.length,
                  itemBuilder: (context, index) {
                    final data = snapshot.data[index];
                    return callbackWidget(
                      context,
                      data,
                    );
                  }),
            );
          }),
    );
  }
}
