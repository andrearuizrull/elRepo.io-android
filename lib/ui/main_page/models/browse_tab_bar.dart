/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:elRepoIo/constants.dart';

/// Page with multiple tabs, used in [bookmarks_page.dart] and
/// [tab_navigation_item.dart].
///
/// Is a Page with a top tab navigator with as much tabs as passed on [myTabs]
///
/// Where [myTabs] are a map with the `key` of the tab, to use it on the
/// controller and the widget associated.
class BrowseXScreen extends StatefulWidget {
  final Map<String, Widget> myTabs;
  final Function(TabController) listener;

  const BrowseXScreen({Key key, @required this.myTabs, this.listener})
      : super(key: key);

  @override
  _BrowseXScreenState createState() => _BrowseXScreenState();
}

class _BrowseXScreenState extends State<BrowseXScreen>
    with SingleTickerProviderStateMixin {
  TabController _tabController;

  final List<Tab> myTabs = [];
  final List<Widget> myWidgets = [];
  @override
  void initState() {
    widget.myTabs.forEach((key, value) {
      myTabs.add(Tab(text: key));
      myWidgets.add(value);
    });
    _tabController = TabController(vsync: this, length: myTabs.length);
    super.initState();
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: myTabs.length,
      child: Builder(builder: (BuildContext context) {
        final _tabController = DefaultTabController.of(context);
        if (widget.listener != null) {
          _tabController.addListener(() {
            widget.listener(_tabController);
          });
        }
        return Scaffold(
          appBar: AppBar(
            toolbarHeight: MediaQuery.of(context).size.height * .01,
            backgroundColor: Theme.of(context).backgroundColor,
            bottom: TabBar(
              controller: _tabController,
              indicatorColor: REMOTE_COLOR,
              labelColor: LOCAL_COLOR,
              unselectedLabelColor: Colors.grey,
              labelPadding: const EdgeInsets.all(1.0),
              tabs: myTabs,
            ),
          ),
          body: TabBarView(
            controller: _tabController,
            children: myWidgets,
          ),
        );
      }),
    );
  }
}
