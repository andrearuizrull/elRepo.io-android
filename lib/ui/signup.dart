/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import 'package:elRepoIo/generated/l10n.dart';
import 'package:elRepoIo/RsServiceControl/rscontrol.dart' as rscontrol;
import 'package:elRepoIo/routes.dart';
import 'package:elRepoIo/constants.dart';
import 'package:elRepoIo/ui/common/loading_dialog.dart';
import 'package:elRepoIo/ui/login.dart';
import 'package:elRepoIo/ui/providers/identities_provider.dart';
import 'package:elRepoIo/user_preferences.dart';
import 'package:flutter/material.dart';
import 'package:file_picker/file_picker.dart';
import 'package:elrepo_lib/repo.dart' as repo;
import 'package:elrepo_lib/constants.dart' as cnst;
import 'package:flutter/foundation.dart' show defaultTargetPlatform, TargetPlatform;
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:elRepoIo/RsServiceControl/rs_folders_control.dart' as RsFolders;


class SignUpScreen extends StatefulWidget {
  @override
  _SignUpScreenState createState() => _SignUpScreenState();
}

class _SignUpScreenState extends State<SignUpScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //backgroundColor: WHITE_COLOR,
      body: Container(
        //margin: defaultMargin(),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Expanded(
              flex: 1,
              child: Container(
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage('assets/bg1.jpg'),
                    fit: BoxFit.cover,
                    alignment: Alignment.bottomCenter,
                  ),
                ),
              ),
            ),
            SignUpForm(),
          ],
        ),
      ),
      /*new Container(
        margin: defaultMargin(),
        child: new ListView(children: <Widget>[new SignUpForm()]),
      ),
      */
    );
  }
}

class SignUpForm extends ConsumerStatefulWidget {
  SignUpForm({Key key}) : super(key: key);

  @override
  _SignUpFormState createState() => _SignUpFormState();
}

class _SignUpFormState extends ConsumerState<SignUpForm> {
  bool _obscureText = true;
  String locationName;
  String password;

  void _toggle() {
    setState(() {
      _obscureText = !_obscureText;
    });
  }

  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Expanded(
        flex: 2,
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16),
          child: Column(
            children: <Widget>[
              Flexible(
                child: Padding(
                  padding: const EdgeInsets.only(top: 12),
                  //TODO refactor "menu", signup and bckp  (btn)
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(
                        S.of(context).signUp,
                        style: TextStyle(
                            color: NEUTRAL_COLOR,
                            fontWeight: FontWeight.bold,
                            fontSize: 21),
                      ),
                      if(defaultTargetPlatform == TargetPlatform.android)
                        InkWell(
                          child: ElevatedButton(
                            style: ElevatedButton.styleFrom(
                              primary: REMOTE_COLOR, // background
                              onPrimary: WHITE_COLOR, // foreground
                            ),
                            onPressed: () async => await _backupFunction(),
                            child: Text(
                              S.of(context).haveABackup,
                              style: TextStyle(color: WHITE_COLOR),
                            ),
                          ),
                        ),
                    ],
                  ),
                ),
              ),
              Spacer(),
              Padding(
                padding: const EdgeInsets.only(bottom: 36),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(right: 14),
                      child: Icon(
                        Icons.person,
                        color: LOCAL_COLOR,
                      ),
                    ),
                    Expanded(
                      child: TextFormField(
                        decoration: InputDecoration(
                          labelText: S.of(context).username,
                        ),
                        validator: (value) {
                          if (value.isEmpty) {
                            return S.of(context).usernameCannotBeEmpty;
                          }
                          return null;
                        },
                        onSaved: (val) => locationName = val,
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 18),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(right: 14),
                      child: Icon(
                        Icons.lock,
                        color: LOCAL_COLOR,
                      ),
                    ),
                    Expanded(
                      child: TextFormField(
                        decoration: InputDecoration(
                          labelText: S.of(context).password,
                          suffix: InkWell(
                            onTap: _toggle,
                            child: Icon(
                              _obscureText
                                  ? Icons.visibility
                                  : Icons.visibility_off,
                              color: LOCAL_COLOR,
                            ),
                          ),
                        ),
                        validator: (value) {
                          if (value.isEmpty) {
                            return S.of(context).passwordCannotBeEmpty;
                          }
                          return null;
                        },
                        onSaved: (val) => password = val,
                        obscureText: _obscureText,
                      ),
                    ),
                  ],
                ),
              ),
              Spacer(),
              Align(
                alignment: Alignment.bottomCenter,
                child: Padding(
                  padding: const EdgeInsets.only(bottom: 17),
                  child: RaisedButton(
                    color: LOCAL_COLOR,
                    textColor: Colors.white,
                    onPressed: () async => await _signupFunction(),
                    child: Center(child: Text(S.of(context).createAccount)),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  /// Signup logic function
  ///
  /// It shows onboarding screen, updatable dialog with the %, and finally, the
  /// edit profile screen for the avatar.
  Future<void> _signupFunction () async {
    // Validate will return true if the form is valid, or false if
    // the form is invalid.
    final form = _formKey.currentState;
    if (form.validate()) {
      form.save();
      //TODO show an status that let users know that their account is creating
      var pr = ProgressDialog(context,
          initialMessage: S.of(context).creatingAccount + ' 0%')
        ..show();

      // Loop to update dialog message while waiting RS signup
      var i = 0;
      Future.doWhile(() async {
        await Future.delayed(Duration(seconds: 1));
        i += 1;
        pr.update(message: S.of(context).creatingAccount + ' $i%');
        if (i > 50) {
          pr.update(message: 'Finishing advanced cryptographic calculations...'
              '\nCould spend a little more' ' 51%'); // todo(intl)
          return false;
        }
        return true;
      });

      // Start signup process
      var signup = repo
          .signUp(password, locationName)
          .then((location) async {
        // Disable signup await loop
        i = 51;

        pr.update(message: 'Setting up friend to friend magic' ' 75%'); // todo(intl)
        if(defaultTargetPlatform == TargetPlatform.android) {
          // we log in the account then stop RetroShare after id creation so data is persisted to avoid
          // problems if the app crashes during the first run.
          // We use this `repo.login` instead of [doLogin] because this second one 
          // have async methods that need RS running all the time
          await repo.login(password, location);
          await rscontrol.RsServiceControlPlatform.instance
              .stopRetroShareExecution();
          await rscontrol.RsServiceControlPlatform.instance
              .startRetroshareLoop();
        }
        pr.update(message: 'Finishing...' ' 90%'); // todo(intl)

        // Do login and prepare the system
        await doLogin(ref, context, RsCredentials(location, password));
        repo.firstPublication(); // Do first publication to start propagate the identity

        // Try to speedup first time CONTENT forums synchronization
        // todo(kon): Use global timer handler. I guess this don't really do
        //  nothing: tiers are not connected yet and synchronization is not
        //  started aready
        // Should be better to move this to a "global" timer and update provider
        // instead calling elRepoLib
        repo.attemptTierConnection().then((value) async {
          repo.subscribeAndSync();
        });
      });

      // And show onboarding screen
      var onboardingScreen = Navigator.of(context).pushNamed('/onboarding');

      // Wait until onboard screen is closed and signup process is done to go to login screen
      await Future.wait([signup, onboardingScreen])
          .whenComplete(() async {
        // Update own identity details to cache it
        ref.read(ownIdentityProvider.notifier).update()
        // Provider.of<OwnIdentityProvider>(context, listen: false).update()
            .then((ownId) async =>
        await Navigator.of(context).pushReplacementNamed(
            '/editprofile',
            arguments: EditProfileArguments(onlyAvatar: true))
        ).whenComplete(() =>  Navigator.pushReplacementNamed(context, '/main'));
      });
    }
  }

  /// Restore a backup
  Future<void> _backupFunction () async {
    // todo(kon): move this logic to elrepo-lib
    // Android only support commented below
    final backupFilePath =
        (await FilePicker.platform.pickFiles())?.files?.single?.path;
    if (backupFilePath != null) {
      var pr = ProgressDialog(context,
          initialMessage: S.of(context).restoreBackup)
        ..show();
      try{
        RsFolders.RsBackup.restoreBackup(backupFilePath,
            (accountStatus) {
              pr.hide();
              if (accountStatus ==
                  cnst.AccountStatus.hasLocation) {
                Navigator.pushReplacementNamed(
                    context, '/login');
              } else {
                showErrorSnackbar();
              }
            });
      } catch (e) {
        pr.hide();
        showErrorSnackbar();
        print(e);
        rethrow;
      }
    }
  }

  void showErrorSnackbar() {
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      content: Text('Error restoring backup'),
      duration: Duration(seconds: 6),
    ));
  }
}
