/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import 'dart:io';
import 'dart:math';

import 'package:elRepoIo/RsServiceControl/rs_folders_control.dart';
import 'package:elRepoIo/constants.dart';
import 'package:elRepoIo/ui/common/download_bar.dart';
import 'package:elRepoIo/ui/common/ui_utils.dart';
import 'package:elRepoIo/ui/providers/teasers_providers.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:retroshare_dart_wrapper/retroshare.dart' as rs;
import 'package:retroshare_dart_wrapper/rs_models.dart' as models;
import 'package:elrepo_lib/repo.dart' as repo;
import 'package:open_file/open_file.dart';
import 'package:path_provider/path_provider.dart';
import 'package:path/path.dart' as path;
import 'package:retroshare_dart_wrapper/rs_models.dart'
    show RetroShareFileDownloadStatus;

import 'package:elRepoIo/generated/l10n.dart';

/// Used to patch https://gitlab.com/elRepo.io/elRepo.io-android/-/issues/48
///
/// Store the file hash when the download start
final _downloadedFiles = <String>[];

/// Card that show to show file information
///
/// [fileLink] contains information about the file, such as fname, fsize, hash...
/// It could be null and
///
/// The [callback] is called on download start pushing the button to start
/// downloading.
class FileDownloadCard extends ConsumerStatefulWidget {
  final String fileHash;
  final String fileLink;
  final Function callback; // Used to update parent view showing progress bar
  final FileDownloadCardController controller;

  const FileDownloadCard(
      {Key key, this.fileHash, this.fileLink, this.callback, this.controller})
      : super(key: key);

  @override
  _FileDownloadCardState createState() => _FileDownloadCardState();
}

class _FileDownloadCardState extends ConsumerState<FileDownloadCard> {
  String _fileHash; // The file hash
  Map<dynamic, dynamic> _fileLinkInfo; // Info parsed by the file link
  // Map<String, dynamic> _fileInfo; // Asyncronous information given by RS backend
  // FileDownloadState _fileState; // File state: downloaded toDownload or downloading
  String _fileName = '';
  int _size = 0;
  FileDownloadCardController _controller;

  void _setState() {
    if (mounted) setState(() {});
  }

  /// Used to get the file info [_fileName] and [_size] from [widget.fileLink]
  ///
  /// Used if [widget.fileLink] is null
  void _setInfoFromFileDetails() {
    if (widget.fileLink == null) {
      _fileName = _controller.fileInfo['fname'];
      _size = _controller.fileInfo['size']['xint64'];
    }
  }

  /// Used to set the card file state [FileDownloadState] using
  /// [RetroShareFileDownloadStatus]
  void _setFileStateFromDownloadStatus(int retroShareFileDownloadStatus) {
    if (retroShareFileDownloadStatus ==
        RetroShareFileDownloadStatus.FT_STATE_WAITING.index ||
        retroShareFileDownloadStatus ==
            RetroShareFileDownloadStatus.FT_STATE_DOWNLOADING.index) {
      fileState = FileDownloadState.DOWNLOADING;
    } else if (retroShareFileDownloadStatus == RetroShareFileDownloadStatus.FT_STATE_PAUSED.index){
      fileState = FileDownloadState.PAUSED;
    } else if (retroShareFileDownloadStatus ==
        RetroShareFileDownloadStatus.FT_STATE_CHECKING_HASH.index) {
      fileState = FileDownloadState.CHECKING_HASH;
    } else if (retroShareFileDownloadStatus ==
        RetroShareFileDownloadStatus.FT_STATE_OKAY.index ||
        retroShareFileDownloadStatus ==
            RetroShareFileDownloadStatus.FT_STATE_COMPLETE.index) {
      fileState = FileDownloadState.DOWNLOADED;
    } else {
      fileState = FileDownloadState.TO_DOWNLOAD;
    }
    print(
        'Set file state for download status $retroShareFileDownloadStatus to: ' +
            _controller.fileState.toString());
  }

  /// Used to ser [_controller.fileState] and update [_setLeadTrailIcons]
  set fileState (FileDownloadState newState) {
    // Only update the state when download is finishing (going from downloading|checking_hash to downloaded)
    // In case download started, this will be called after bookmarking a page, because is needed to update
    // bookmarks first
    if(newState == FileDownloadState.DOWNLOADED &&
        (_controller.fileState == FileDownloadState.CHECKING_HASH
            || _controller.fileState == FileDownloadState.DOWNLOADING
        )
    ){
      ref.watch(allBookmarksProvider.notifier).update();
    }
    _controller.fileState = newState;
    _setLeadTrailIcons();
    _setState();
  }

  /// Set controller file info
  ///
  /// It may differ depending if is a already downloaded file or a downloading/
  /// toDownload file because we extract the info using
  /// [rs.RsFiles.alreadyHaveFile] or [rs.RsFiles.fileDetails]
  void _setFileInfo(Map<String, dynamic> fileInfo, String absolutePath) {
    print('File info card: $fileInfo');
    _controller.fileInfo = fileInfo;
    _controller.absolutePath = absolutePath;
  }

  /// Using file hash get file info from RsFiles.fileDetails and set the card
  /// state from fileInfo["downloadStatus"]
  Future<void> _getFileInfoFromFileHash(fileHash) async {
    _fileHash = widget.fileHash;

    // Check if the file is downloading, downloaded or to download
    // Case already downloaded
    // todo(kon): found a way to do this in only one call (for example call to
    // fileDetails). I tried to do it before but I get some error that I didn't
    // documented, probably related with fileDetails hintflags
    rs.RsFiles.alreadyHaveFile(_fileHash).then((fileInfo) {
      // Case not downloaded
      if (fileInfo == null || fileInfo['path'] == null) {
        print('alreadyHaveFile failed for $_fileHash');
        rs.RsFiles.fileDetails(_fileHash).then((fileDetails) async {

          // This is a patch for https://gitlab.com/elRepo.io/elRepo.io-android/-/issues/48
          if(fileDetails['hash'] != _fileHash && _downloadedFiles.contains(_fileHash)) {
            print(fileDetails);
            await Future.delayed(Duration(seconds: 1));
            _getFileInfoFromFileHash(fileHash);
            return;
          }

          _setFileInfo(fileDetails, fileDetails['path'] + '/' + fileDetails['fname']);
          _setFileStateFromDownloadStatus(fileDetails['downloadStatus']);
          _setInfoFromFileDetails();
          _setState();
        });
      } else {
        print('alreadyHaveFile success for $_fileHash $fileInfo');
        _setFileInfo(fileInfo, fileInfo['path']);
        _setFileStateFromDownloadStatus(
            RetroShareFileDownloadStatus.FT_STATE_COMPLETE.index);
        _setInfoFromFileDetails();
        _setState();
        if (widget.controller != null) widget.controller.setFileExists(true);
      }
    });
  }

  /// From a file link it set [_fileLinkInfo] [_fileName] and [_size]
  // todo(kon): move this to elrepo-lib
  void _parseFileLink(String fileLink) {
    print("Post file link: $fileLink");
    rs.RsFiles.parseFilesLink(fileLink).then((parsed) {
      _fileLinkInfo = parsed;
      _fileName = parsed['mFiles'][0]['name'];
      _size = parsed['mFiles'][0]['size']['xint64'];
      _setState();
    });
  }

  /// Get the subtitle of the card depending on the card status
  Widget _getSubtitle() {
    if (!(_size > 0)) return Text(S.of(context).fileDownloadCardLoadingInfo);
    var textBytes =
    Text(_formatBytes(_size, 2), style: TextStyle(color: REMOTE_COLOR));
    if (_controller.fileState == FileDownloadState.CHECKING_HASH) {
      return Row(
        children: [
          textBytes,
          OpacityBlinkingText(Text(S.of(context).fileDownloadCardHashing,
              style: TextStyle(color: REMOTE_COLOR))),
        ],
      );
    }
    return textBytes;
  }

  Widget _leadingIcon = Icon(Icons.hourglass_bottom),
      _trailingIcon;

  /// Get the leading widget for the card depending on card state
  void _setLeadTrailIcons() {
    if (_controller.fileState == FileDownloadState.DOWNLOADED) {
      _leadingIcon = OpenPayloadWidget(
        repo.getPathIfExists(_fileHash),
        icon: Icon(Icons.open_in_new, color: LOCAL_COLOR),
      );
      _trailingIcon = DeleteFile(_fileHash,
          icon: Icon(Icons.delete),
          color: RED_COLOR,
          cb: () {
            fileState = FileDownloadState.TO_DOWNLOAD;
            _setState();
          }
      );
    } else if (_controller.fileState == null) {
      _leadingIcon = IconButton(
        icon: Icon(Icons.hourglass_bottom),
        color: REMOTE_COLOR,
        onPressed: () {},
      );
      _trailingIcon = null;
    } else if (_controller.fileState == FileDownloadState.DOWNLOADING
        || _controller.fileState == FileDownloadState.PAUSED) {
      _leadingIcon =  PlayPauseDownload(_fileHash, _controller.fileState,
          onChanged: (bool isDownloading) {
            isDownloading
                ? fileState = FileDownloadState.DOWNLOADING
                : fileState = FileDownloadState.PAUSED;
            _setState();
          }
      );
      _trailingIcon = CancelDownload(_fileHash,
        icon: Icon(Icons.cancel),
        color: NEUTRAL_COLOR,
        cb: (){
          fileState = FileDownloadState.TO_DOWNLOAD;
          _setState();
        },
      );
    }
    else if(_controller.fileState == FileDownloadState.CHECKING_HASH) {
      _leadingIcon = IconButton(
        icon: Icon(Icons.cloud_download_outlined),
        color: NEUTRAL_COLOR,
        onPressed: () {},
      );
      _trailingIcon = null;
    } else {
      _leadingIcon = IconButton(
        icon: Icon(Icons.cloud_download),
        color: REMOTE_COLOR,
        onPressed: _requestFiles,
      );
      _trailingIcon = null;
    }
  }

  /// Request files to RetroShare
  void _requestFiles () async {
    if(_controller.fileState != null && !_controller.fileExists &&
        _controller.fileState != FileDownloadState.DOWNLOADING &&
        _controller.fileState != FileDownloadState.PAUSED &&
        _controller.fileState != FileDownloadState.CHECKING_HASH) {

      if(defaultTargetPlatform == TargetPlatform.android) {
        await RsFoldersControl.requestFilePermissions();
      }
      // Get file link to RetroShare
      _fileLinkInfo != null
          ? rs.RsFiles.requestFiles(_fileLinkInfo)
          :
      // Below is used when no _fileLinkInfo is provided but we have the _fileInfo
      rs.RsFiles.exportFileLink(
          _controller.fileInfo['hash'],
          _controller.fileInfo['size']['xint64'],
          _controller.fileInfo['fname'])
          .then((link) => rs.RsFiles.parseFilesLink(link)
          .then((value) => rs.RsFiles.requestFiles(value)));

      // Call callback, usually to bookmark or SetState outside this widget
      if (widget.callback != null) widget.callback();

      // Finally set the state for this widget
      fileState = FileDownloadState.DOWNLOADING;

      // Used to patch https://gitlab.com/elRepo.io/elRepo.io-android/-/issues/48
      if(!_downloadedFiles.contains(_fileHash)) _downloadedFiles.add(_fileHash);
      _setState();
    }
  }

  @override
  void initState() {
    if (widget.fileHash != null) _getFileInfoFromFileHash(widget.fileHash);
    if (widget.fileLink != null) _parseFileLink(widget.fileLink);
    if (widget.controller != null) widget.controller.requestFiles = _requestFiles;
    _controller = widget.controller ?? FileDownloadCardController();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Card(
          child: Column(
            children: [
              ListTile(
                leading: _leadingIcon,
                title: _fileName != ''
                    ? InkWell(
                    onTap: () async {
                      print('tapped on $_fileName');
                      if(_controller.fileState == FileDownloadState.DOWNLOADED) {
                        await OpenFile.open(await repo.getPathIfExists(_fileHash));
                      }
                      else if(_controller.fileState != null &&
                          _controller.fileState != FileDownloadState.DOWNLOADING &&
                          _controller.fileState != FileDownloadState.PAUSED &&
                          _controller.fileState != FileDownloadState.CHECKING_HASH) {
                        _requestFiles();
                      }
                    },
                    child: Text('${path.basename(_fileName)}',
                        style: TextStyle(color: REMOTE_COLOR)))
                    : Text(''),
                subtitle: _getSubtitle(),
                trailing: _trailingIcon,
              ),
            ],
          ),
        ),
        // Download Bar
        if (_controller.fileState == FileDownloadState.DOWNLOADING ||
            _controller.fileState == FileDownloadState.PAUSED ||
            _controller.fileState == FileDownloadState.CHECKING_HASH)
          DownloadBar(
            downloadingHash: _fileHash,
            onDone: (downloadedPath) async {
              // Update the icon
              if (downloadedPath != null) {
                // Update the icon
                _getFileInfoFromFileHash(_fileHash);
                // We supose that if we arrive here but we don't have the path
                // already is because is checking hash.
              } else {
                if (_controller.fileState != FileDownloadState.CHECKING_HASH) {
                  _setFileStateFromDownloadStatus(RetroShareFileDownloadStatus
                      .FT_STATE_CHECKING_HASH.index);
                  _setState();
                }
              }
              _setState();
            },
          ),
      ],
    );
  }
}

String _formatBytes(int bytes, int decimals) {
  if (bytes <= 0) return '0 B';
  const suffixes = ['B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
  var i = (log(bytes) / log(1024)).floor();
  return ((bytes / pow(1024, i)).toStringAsFixed(decimals)) + ' ' + suffixes[i];
}

class FileDownloadCardController extends ChangeNotifier {
  bool fileExists = false;
  FileDownloadState fileState;
  Map<String, dynamic> fileInfo;
  String absolutePath;
  void Function() requestFiles;

  void setFileExists(bool value) {
    fileExists = value;
    notifyListeners();
  }
}

enum FileDownloadState {
  DOWNLOADED,
  DOWNLOADING,
  TO_DOWNLOAD,
  CHECKING_HASH, // Used when a file is 100% but rs don't changed the status yet
  PAUSED,
}

class OpenPayloadWidget extends StatelessWidget {
  final Future<String> filePath;
  final Icon icon;
  OpenPayloadWidget(this.filePath, {@required this.icon});

  @override
  Widget build(BuildContext context,) {
    return IconButton(
        icon: icon,
        onPressed: () {
          filePath.then((path) async {
            if (path != null) {
              print('Opening file $path');
              // Check if is a network file
              // todo(kon): this is needed?
              if (path.startsWith(rs.getRetroshareServicePrefix().substring(
                  0, rs.getRetroshareServicePrefix().lastIndexOf('/')))) {
                var netPath = path;
                var tempDir = await getTemporaryDirectory();
                path = tempDir.path + '/' + path.split('/').last;

                print('Saving $netPath network file on: $path');

                await http.get(Uri.parse(netPath)).then((response) {
                  File(path).writeAsBytes(response.bodyBytes);
                });
              }
              final openStatus = await OpenFile.open(path);
              print('Open file result: ${openStatus.message}');
            } else {}
          });
        });
  }
}

class PlayPauseDownload extends StatefulWidget {
  final FileDownloadState initialState;
  final Function(bool) onChanged;
  final String hash;

  const PlayPauseDownload(this.hash, this.initialState, {this.onChanged, Key key}) : super(key: key);

  @override
  _PlayPauseDownloadState createState() => _PlayPauseDownloadState();
}

class _PlayPauseDownloadState extends State<PlayPauseDownload> with TickerProviderStateMixin {

  AnimationController _animationController;
  bool isDownloading, // If isDownloading "pause" button zshpould be shown
  /// Used to store the button initial state because depending on
  /// [widget.initialState] it uses [AnimatedIcons.play_pause] or
  /// [AnimatedIcons.pause_play]
      _buttonInitialState = true;
  AnimatedIconData _iconData;

  @override
  void initState() {
    super.initState();
    _animationController =
        AnimationController(vsync: this, duration: Duration(milliseconds: 450));
    if (widget.initialState == FileDownloadState.PAUSED) {
      _iconData = AnimatedIcons.play_pause;
      isDownloading = false;
    } else {
      _iconData = AnimatedIcons.pause_play;
      isDownloading = true;
    }
  }

  @override
  Widget build(BuildContext context) {
    return IconButton(
      icon: AnimatedIcon(
        icon: _iconData,
        progress: _animationController,
        color: NEUTRAL_COLOR,
      ),
      onPressed: () async {

        var res = isDownloading
            ? await rs.RsFiles.FileControl(widget.hash, models.RS_FILE_CTRL.PAUSE)
            : await rs.RsFiles.FileControl(widget.hash, models.RS_FILE_CTRL.START);

        setState(() {
          if(mounted) {
            isDownloading = !isDownloading;

            _buttonInitialState
                ? _animationController.forward()
                : _animationController.reverse();
            _buttonInitialState = !_buttonInitialState;

            widget.onChanged(isDownloading);
          }
        });
      },
    );
  }
}

class CancelDownload extends StatelessWidget {
  final String fileHash;
  final Icon icon;
  final Function cb;
  final Color color;
  const CancelDownload(this.fileHash,{Key key, @required this.icon, this.cb, this.color}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return IconButton(
      icon: icon,
      color: color,
      onPressed: () async {
        confirmDialog( context,
            title: S.of(context).fileDownloadCardCancelDownload,
            accept: S.of(context).yes, //
            cancel: S.of(context).cancel,
            content: Text(S.of(context).fileDownloadCardCancelDownloadConfirmation), // todo(intl)
            callback: (accepted) async {
              if(accepted) {
                await rs.RsFiles.FileCancel(fileHash);
                if(cb != null) cb();
              }
            });
      },
    );
  }
}

class DeleteFile extends StatelessWidget {
  final String fileHash;
  final Icon icon;
  final Function cb;
  final Color color;
  const DeleteFile(this.fileHash,{Key key, @required this.icon, this.cb, this.color}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return IconButton(
      icon: icon,
      color: color,
      onPressed: () async {
        confirmDialog( context,
            title: S.of(context).fileDownloadDeleteFile,
            accept: S.of(context).yes,
            cancel: S.of(context).cancel,
            content: Text(S.of(context).fileDownloadDeleteFileConfirmation),
            callback: (accepted) async {
              if (accepted) {
                final path = await repo.getPathIfExists(fileHash);
                try {
                  await File(path).delete();
                  await rs.RsFiles.forceDirectoryCheck();
                  await rs.RsFiles.forceDirectoryCheck(add_safe_delay: false);
                  if(cb != null) cb();
                } catch (e) {
                  print('Can\'t delete file $path');
                  Scaffold.of(context).showSnackBar(
                      SnackBar(
                        content: Text('Error deleting file!'), // todo(intl)
                        duration: Duration(seconds: 6),
                      )
                  );
                  rethrow;
                }
              }
            }
        );
      },
    );
  }
}
