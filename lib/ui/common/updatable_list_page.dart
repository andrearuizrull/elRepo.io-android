/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import 'dart:async';

import 'package:elRepoIo/ui/common/ui_utils.dart';
import 'package:elRepoIo/ui/providers/models.dart';
import 'package:elRepoIo/ui/providers/timers_provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

/// Page to show a list of posts teasers
///
/// Use a [provider] of type [TeaserListNotifier] to show it. This
/// [TeaserListNotifier] implements an update function.
///
/// The [callbackWidget] parameter is the widget that will consume the provider
/// data.
///
/// Optionally, you can specify  a provider that will perform the update on the
/// pull down to refresh (using the [provider] parameter) and a provider to show
/// on the list (using [listProvider]). This is util when a provider updates
/// other, for example when updating bookmarks provider we also update the
/// downloading provider.
///
/// If [timerCallback] is not null, it will start a timer each
/// [timerDelay]. The timer will stop on dispose or canceling it using the
/// [timerCallback] that receive the instantiated [Timer] and the 
/// [WidgetRef].
///
/// [noDataWidget] is a function that provides a widget to show when the
/// provider is empty. It receive the WidgetRef as argument.
class UpdatableListPage<T> extends ConsumerStatefulWidget {
  final StateNotifierProvider<UpdatableNotifier, List<T>> provider;
  final StateProvider listProvider;
  final Widget Function(T t) callbackWidget;
  final Function(WidgetRef, Timer) timerCallback;
  final Duration timerDelay;
  final Widget Function(WidgetRef) noDataWidget;
  final BoxDecoration backgroundDecoration;
  final Axis scrollDirection;

  const UpdatableListPage(this.provider, {
    @required this.callbackWidget,
    this.listProvider,
    this.timerCallback,
    this.timerDelay = const Duration(seconds: 5),
    this.noDataWidget,
    this.backgroundDecoration,
    this.scrollDirection = Axis.vertical,
    Key key
  })
      : assert(callbackWidget != null), super(key: key);

  @override
  _UpdatableListPageState<T> createState() => _UpdatableListPageState<T>();
}

class _UpdatableListPageState<T> extends ConsumerState<UpdatableListPage<T>> {
  final _refreshKey = GlobalKey<RefreshIndicatorState>();

  Timer timer;

  void startAutoUpdateTimer() {
    timer = ref.read(timersProvider.notifier).addTimer(
        Timer.periodic(Duration(seconds: 3), (timer) {
      widget.timerCallback(ref, timer);
    }));
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance
        .addPostFrameCallback((_) {
      if (widget.timerCallback != null) startAutoUpdateTimer();
    });
  }

  @override
  void dispose() {
    timer?.cancel();
    super.dispose();
  }

  void update(){
    ref.read(widget.provider.notifier).update();
  }

  @override
  Widget build(BuildContext context) {
    // You can specify a provider to update and a provider to watch. For
    // example, the bookmarks provider update also updates the downloading
    // provider. So, to update the downloading provider, we have to update the
    // [widget.provider] and show the [widget.listProvider]
    var modelList = widget.listProvider != null
        ? ref.watch(widget.listProvider) : ref.watch(widget.provider);

    return Container(
      decoration: widget.backgroundDecoration,
      child: RefreshIndicator(
        onRefresh: () async {
          // ref.refresh(widget.provider);
          update();
        },
        child: Container(
          child:
            modelList.isEmpty?
              Center(
                child: Column(
                  children: [
                    widget.noDataWidget != null
                        ? widget.noDataWidget(ref)
                        : NoDataYetButton(callback: () => update()),
                  ],
                ),
              )
            : ListView(
                physics: ClampingScrollPhysics(),
                key: _refreshKey,
                shrinkWrap: true,
                scrollDirection: widget.scrollDirection,
                children: [
                  for (final product in modelList as List<T>) widget.callbackWidget.call(product),
              ],
            ),
        ),
      ),
    );
  }
}