/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import 'package:elRepoIo/constants.dart';
import 'package:elRepoIo/routes.dart';
import 'package:flutter/material.dart';
import 'package:flutter_parsed_text/flutter_parsed_text.dart';
import 'package:retroshare_dart_wrapper/rs_models.dart';
import 'package:url_launcher/url_launcher.dart';

/// Used to parse links, hashtags and other stuff on a multiline text
///
/// Actually parses: #Hashtags, Urls, mails
///
/// You can provide a [postMetadata] in order to show the origin post at least
/// when click on a hashtag
class ElRepoioParsedText extends StatelessWidget {
  final String text;
  final TextStyle style;
  final RsMsgMetaData postMetadata;
  const ElRepoioParsedText(this.text, {this.style, this.postMetadata,Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ParsedText(
      text: text,
      style: style ?? Theme.of(context).textTheme.bodyText2,
      regexOptions: RegexOptions(
        multiLine: true,
      ),
      parse: <MatchText>[
        // ElRepo.io hashtags
        MatchText(
          pattern: r'#\S+',
          style: TextStyle(
            color: REMOTE_COLOR,
            letterSpacing: 0.60,
            fontWeight: FontWeight.w600,
            decoration: TextDecoration.underline,
          ),
          onTap: (url) async {
            _openHashtag(url, context);
          },
        ),
        // Urls
        MatchText(
          pattern: r'https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()!@:%_\+.~#?&\/\/=]*)',
          style: TextStyle(
            color: REMOTE_COLOR,
            // letterSpacing: 1.0,
          ),
          onTap: (url) async {
            _openUrl(url);
          },
        ),
        // Mail
        MatchText(
          pattern: r'[^@ \t\r\n]+@[^@ \t\r\n]+\.[^@ \t\r\n]+',
          style: TextStyle(
            color: REMOTE_COLOR,
            // letterSpacing: 1.0,
          ),
          onTap: (url) async {
            _openUrl(url);
          },
        ),
      ],
    );
  }
  Future<void> _openUrl(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  void _openHashtag(String hashtag, BuildContext context) {
    Navigator.pushNamed(context, '/browsetag',
        arguments: TagArguments(hashtag.split('#')[1], postMetadata),);
  }
}
